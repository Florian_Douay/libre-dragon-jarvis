OPERATORS_DECLARATION
  {addition,+,"./ressources/img_operators/addition.png",2}
  {moinsUnaire,!,"./ressources/img_operators/moins_unaine.png",1}
  {soustraction,-,"./ressources/img_operators/soustraction.png",2}
  {multiplication,*,"./ressources/img_operators/multiplication.png",2}
  {division,/,"./ressources/img_operators/division.png",2}
  {egal,=,"./ressources/img_operators/egal.png",2}
LOCAL_RULES
  { ( a + (b + c) ) # ( (a + b) + c ) }
  { ( a + 0 ) # ( a ) }
  { ( 0 + a ) # ( a ) }
  { ( (!a) + 0 ) # ( !a ) }
  { ( 0 + (!a) ) # ( !a ) }
  { ( a + (!a) ) # ( 0 ) }
  { ( (!a) + a ) # ( 0 ) }
  { ( !(!a) ) # ( a ) }
  { ( a - b ) # ( b - a ) }
  { ( a - (b - c) ) # ( (a - b) - c ) }
  { ( a - 0 ) # ( a ) }
  { ( 0 - a ) # ( !a ) }
  { ( (!a) - 0 ) # ( !a ) }
  { ( 0 - (!a) ) # ( a ) }
  { ( a - (!a) ) # ( 0 ) }
  { ( (!a) - a ) # ( 2 * (!a) ) }
  { ( a * b ) # ( b * a ) }
  { ( (a * b) * c ) # ( a * (b * c) ) }
  { ( a * (b + c) ) # ( (a * b) + (a * c) ) }
  { ( (a + b) * c ) # ( (a * c) + (b * c) ) }
  { ( a * (b - c) ) # ( (a * b) - (a * c) ) }
  { ( (a - b) * c ) # ( (a * c) - (b * c) ) }
  { ( (!a) * (!a) ) # ( a * a ) }
  { ( a * 0 ) # ( 0 ) }
  { ( 0 * a ) # ( 0 ) }
  { ( (!a) * 0 ) # ( 0 ) }
  { ( 0 * (!a) ) # ( 0 ) }
  { ( a * 1 ) # ( a ) }
  { ( 1 * a ) # ( a ) }
  { ( (!a) * 1 ) # ( !a ) }
  { ( 1 * (!a) ) # ( !a ) }
  { ( a * (!1) ) # ( !a ) }
  { ( (!1) * a ) # ( !a ) }
  { ( (a / b) / c ) # ( a / (b * c) ) }
  { ( a / (b / c) ) # ( (a * c) / b ) }
  { ( ((!a) / b) / c ) # ( (!a) / (b * c) ) }
  { ( (!a) / (b / c) ) # ( ((!a) * c) / b ) }
  { ( (a / (!b)) / c ) # ( a / ((!b) * c) ) }
  { ( a / ((!b) / c) ) # ( (a * c) / (!b) ) }
  { ( (a / b) / (!c) ) # ( a / (b * (!c)) ) }
  { ( a / (b / (!c)) ) # ( (a * (!c)) / b ) }
  { ( ((!a) / b) / (!c) ) # ( (!a) / (b * (!c)) ) }
  { ( (!a) / (b / (!c)) ) # ( ((!a) * (!c)) / b ) }
  { ( (a / (!b)) / (!c) ) # ( a / ((!b) * (!c)) ) }
  { ( a / ((!b) / (!c)) ) # ( (a * (!c)) / (!b) ) }
  { ( ((!a) / (!b)) / c ) # ( (!a) / ((!b) * c) ) }
  { ( (!a) / ((!b) / c) ) # ( ((!a) * c) / (!b) ) }
  { ( ((!a) / (!b)) / (!c) ) # ( (!a) / ((!b) * (!c)) ) }
  { ( (!a) / ((!b) / (!c)) ) # ( ((!a) * (!c)) / (!b) ) }
  { ( 0 / a ) # ( 0 ) }
  { ( 0 / (!a) ) # ( 0 ) }
  { ( a / 1 ) # ( a ) }
  { ( (!a) / 1 ) # ( !a ) }
  { ( a / (!1) ) # ( !a ) }
  { ( (!a) / (!1) ) # ( a ) }
  { ( (a + b) / c ) # ( (a / c) + (b / c) ) }
  { ( (a * b) / c ) # ( a * (b / c) ) }
  { ( a / (b * c) ) # ( (a / b) * (1 / c) ) }
GLOBAL_RULES
  { (a + b) # (c) ## ( (a + b) / d ) # (c / d) }
  { (a + b) # (c) ## ( (a + b) * d ) # (c * d) }
