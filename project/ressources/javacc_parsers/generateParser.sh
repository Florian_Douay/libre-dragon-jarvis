#You need to have installed javacc on your system like
#apt-get install javacc
#or
#dnf install javacc

#(re)generate parser java classes for operators and rules
/usr/bin/javacc.sh operatorsAndRules.jj

#(re)generate parser java classes for game levels
/usr/bin/javacc.sh gameLevel.jj
