package managers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import modele.metier.BinaryExpression;
import modele.metier.Equation;
import modele.metier.Expression;
import modele.metier.GlobalRule;
import modele.metier.LiteralConstant;
import modele.metier.Operator;
import modele.metier.LocalRule;
import modele.metier.TerminalExpression;
import modele.metier.UnaryExpression;
import modele.metier.Variable;

/**
 * Manager that operates actions on the model of current level
 * <p>
 * It allows :
 * <ul>
 * <li>To retrieve all possible form changes on a given model path</li>
 * <li>To retrieve all possible form changes on the global equation of the current level</li>
 * <li>To apply a local rule on a given model path</li>
 * <li>To apply a global rule on the global equation of the current level</li>
 * <li>To apply a global operation with operator and its argument(s) on the global equation of the current level</li>
 * </ul>
 * 
 * @since 1.0
 * @see GameLevelManager
 * @see GameLevel
 * @see Equation
 * @see LocalRule
 * @see GlobalRule
 * @see Operator
 * 
 */

public class RulesApplicationManager {
	
	private static RulesApplicationManager INSTANCE = new RulesApplicationManager();

	/**
	 * Returns the unique instance of the class.
	 * @return the unique instance of the class.
	 */
	public static RulesApplicationManager getInstance () {
		return INSTANCE;
	}
	
	/**
	 * Private default constructor.
	 * Is only used on initializing the static instance of the class.
	 */
	private RulesApplicationManager() {

	}
	
	/**
	 * Returns all possible local rules that can be applied to the given model path (would modify one element in one side of the equation).
	 * @param modelPath the model path where to check possible rules.
	 * @return an array of array of string representing all possible rules. null if the retrieving operation failed. empty if zero results.
	 */
	public String[][] getPossibleFormChanges(String modelPath) {
		
		//Checks if a level is currently loaded
		if (GameLevelManager.getInstance().isAGameLevelLoaded() == false) {
			System.err.println("Can't retrieve possible form changes. No level is currently loaded");
			return null;
		}
		
        //Checks if the given modelPath exists in the current game level's model
        if (GameLevelManager.getInstance().isPathExistingInModel(modelPath) == false) {
    		System.err.println("Can't retrieve possible form changes. The given modelPath doesn't exist in currentGameLevel");
    		return null;
        }
        
        HashMap<Integer,LocalRule> resultMap = getAllMatchingLocalRulesOnModelPath(modelPath);
  
        //Creates the String[][] for the final result to return
        String[][] result = new String[resultMap.size()][2];
        
        //Fills the final String[][] from intermediate result hashMap entries
        int indexCounter = 0;
        for (Map.Entry<Integer,LocalRule> resultRuleEntry : resultMap.entrySet()) {

        	result[indexCounter][0] = Integer.toString(resultRuleEntry.getKey());
        	result[indexCounter++][1] = resultRuleEntry.getValue().toString();
        }
        
        return result;
	}

	/**
	 * Returns all possible global rules that can be applied to the current game level equation (would modify both sides of the equation).
	 * @return an array of array of string representing all possible rules. null if the retrieving operation failed. empty if zero results.
	 */
	public String[][] getPossibleGlobalFormChanges() {
		
		//Checks if a level is currently loaded
		if (GameLevelManager.getInstance().isAGameLevelLoaded() == false) {
			System.err.println("Can't retrieve possible global form changes. No level is currently loaded");
			return null;
		}
        
        HashMap<Integer,GlobalRule> resultMap = getAllMatchingGlobalRules();
  
        //Creates the String[][] for the final result to return
        String[][] result = new String[resultMap.size()][2];
        
        //Fills the final String[][] from intermediate result hashMap entries
        int indexCounter = 0;
        for (Map.Entry<Integer,GlobalRule> resultRuleEntry : resultMap.entrySet()) {

        	result[indexCounter][0] = Integer.toString(resultRuleEntry.getKey());
        	result[indexCounter++][1] = resultRuleEntry.getValue().toString();
        }
        
        return result;
	}
	
	/**
	 * Tries to apply the local rule corresponding to the given id to the given model path (modifying one element in one side of the equation).
	 * @param ruleIdToApply the id of the rule to apply.
	 * @param modelPathWhereToApply the model path where to apply the local rule.
	 * @return true if the rule has been applied successfully. false otherwise.
	 */
	public boolean applyLocalRule(int ruleIdToApply, String modelPathWhereToApply) {
		
		//Checks if a level is currently loaded
		if (GameLevelManager.getInstance().isAGameLevelLoaded() == false) {
			System.err.println("Can't apply the local rule. No level is currently loaded");
			return false;
		}
		
        //Checks if the given modelPath exists in the current game level's model
        if (GameLevelManager.getInstance().isPathExistingInModel(modelPathWhereToApply) == false) {
    		System.err.println("Can't apply the local rule. The given modelPath doesn't exist in currentGameLevel");
    		return false;
        }
		
        //Retrieves the main rule container
        HashMap<Integer,LocalRule> mainRuleMap = GameManager.getInstance().getLocalRuleMap();
        
        //Retrieves the rule corresponding to the given ruleId. No need to test if it's a global rule or a local rule : they are in separated container with global unique ids
        LocalRule ruleToApply = mainRuleMap.get(ruleIdToApply);
        
        //Checks if the given ruleId correspond to an existing rule
        if (ruleToApply == null) {
    		System.err.println("Can't apply the local rule. The given ruleId is invalid");
    		return false;
        }
        
        //Tries to apply the local rule
        if (applyLocalRule(ruleToApply, modelPathWhereToApply) == false) {
        	System.err.println("Can't apply the local rule. The level equation is not loaded or the rule is not applicable");
        	return false;
        }
        else {
        	//Notifies observers that the model has changed
        	GameLevelManager.getInstance().notifySubscribedObservers();
        	return true;
        }
	}
	
	/**
	 * Tries to apply the global rule corresponding to the given id to the entire equation of current game level (modifying both sides of the equation).
	 * @param ruleIdToApply the id of the rule to apply.
	 * @return true if the rule has been applied successfully. false otherwise.
	 */
	public boolean applyGlobalRule(int ruleIdToApply) {
		
		//Checks if a level is currently loaded
		if (GameLevelManager.getInstance().isAGameLevelLoaded() == false) {
			System.err.println("Can't apply the global rule. No level is currently loaded");
			return false;
		}
		
        //Retrieves the main rule container
        HashMap<Integer,GlobalRule> mainRuleMap = GameManager.getInstance().getGlobalRuleMap();
        
        //Retrieves the rule corresponding to the given ruleId. No need to test if it's a global rule or a local rule : they are in separated container with global unique ids
        GlobalRule ruleToApply = mainRuleMap.get(ruleIdToApply);
        
        //Checks if the given ruleId correspond to an existing rule
        if (ruleToApply == null) {
    		System.err.println("Can't apply the global rule. The given ruleId is invalid");
    		return false;
        }
        
        //Tries to apply the global rule
        if (applyGlobalRule(ruleToApply) == false) {
        	System.err.println("Can't apply the global rule. The level equation is not loaded or the rule is not applicable");
        	return false;
        }
        else {
        	//Notifies observers that the model has changed
        	GameLevelManager.getInstance().notifySubscribedObservers();
        	return true;
        }
	}
	
	/**
	 * Tries to apply the given operator and its eventual arguments to the entire equation of current game level (modifying both sides of the equation)..
	 * @param operatorSymbol the symbol of the operator.
	 * @param operatorArgs eventual arguments for the given operator.
	 * @return true if the operation has been done successfully. false otherwise.
	 */
	public boolean applyOperation(String operatorSymbol, String[] operatorArgs) {
		
		//Checks if a level is currently loaded
		if (GameLevelManager.getInstance().isAGameLevelLoaded() == false) {
			System.err.println("Can't retrive possible operations. No level is currently loaded");
			return false;
		}
		
        //Checks if the given operator is allowed by this level
		if (GameLevelManager.getInstance().getCurrentLevelOperators().get(operatorSymbol) == null) {
        	System.err.println("Can't retrive possible operations. The given operator is not allowed by this level");
        	return false;
		}
		
		//Tries to retrieves operator instance
		Operator operator = GameManager.getInstance().getOperatorMap().get(operatorSymbol);
		
		//Checks the validity of retrieved operator instance
        if(operator == null)
        {
        	System.err.println("Can't retrive possible operations. Given operator doesn't exist");
        	return false;
        }
        
        //Checks if the number of arguments is right according to the operator
        if (checkOperatorArguments(operator, operatorArgs) == false) {
        	System.err.println("Can't retrive possible operations. The number of arguments is incorrect.");
        	return false;
        }
        
    	Equation currentLevelEquation = GameLevelManager.getInstance().getEquationOfRunningGameLevel();
    	
    	if (currentLevelEquation == null) {
    		System.err.println("Can't retrive possible operations. Can't access to the current level's equation. The level is probably not loaded");
    		return false;
    	}
        
        if(operator.getArity() == 1) {
        	
        	if (applyGlobalUnaryOperation(currentLevelEquation, operator) == false) {
        		System.err.println("Can't retrive possible operations. applyGlobalUnaryOperation() failed.");
        		return false;
        	}
        	else {
        		//Notifies observers that the model has changed
        		GameLevelManager.getInstance().notifySubscribedObservers();
        		return true;
        	}
        }
        
        else if(operator.getArity() == 2) {
    		
        	//No need to test operatorArgs : the function checkOperatorArguments() already check it
        	TerminalExpression terminalExpression = retrieveTerminalExpressionFromArg(operatorArgs[0]);
        
        	//Checks if the argument is allowed by the current level
        	if (isAllowedByLevel(terminalExpression) == false) {
        		System.err.println("Can't retrive possible operations. The given argument to binaryOperator is not allowed by this level");
        		return false;
        	}
            
        	//Applies the binary expression to the global equation (left and right part)
        	if (applyGlobalBinaryOperation(currentLevelEquation, operator, terminalExpression) == false) {
        		System.err.println("Can't retrive possible operations. applyGlobalBinaryOperation() failed.");
        		return false;
        	}
        	else {
        		//Notifies observers that the model has changed
        		GameLevelManager.getInstance().notifySubscribedObservers();
        		return true;
        	}
        }
        else {
        	System.err.println("Can't retrive possible operations. The system currently only manage binary and unary expressions. Operator has been badly defined");
        	return false;
        }

	}
	
	private HashMap<Integer,LocalRule> getAllMatchingLocalRulesOnModelPath(String modelPath){
		
        //Retrieves the main rule container
        HashMap<Integer,LocalRule> mainLocalRuleMap = GameManager.getInstance().getLocalRuleMap();
        
        //Creates the intermediate result hashMap
        HashMap<Integer,LocalRule> resultMap = new HashMap<Integer,LocalRule>();
        
        //Retrieves currentLevelEquation
        Equation currentLevelEquation = GameLevelManager.getInstance().getEquationOfRunningGameLevel();
        
        //Checks if the equation has been retrieved successfully
		if (currentLevelEquation == null) {
			System.err.println("Can't get all matching local rules on model path. Failed to retrieve equation of current game level");
			return null;
		}
        
        //Tries every rules on this modelPath
        for (Map.Entry<Integer,LocalRule> currentLocalRule : mainLocalRuleMap.entrySet()) {
        	
        	//If the current rule is applicable to modelPath, stocks it in intermediate result hashMap
        	//case matching left part of the rule
        	if( currentLevelEquation.isLocalRuleValid(modelPath, currentLocalRule.getValue().getEquation()) )
        	{
        		resultMap.put(currentLocalRule.getKey(), currentLocalRule.getValue());
        	}
        	//case matching right part of the rule
        	else if( currentLevelEquation.isLocalRuleValid(modelPath, currentLocalRule.getValue().getEquation().getSwappedInstance()) )
        	{
        		resultMap.put(currentLocalRule.getKey(), currentLocalRule.getValue());
        	}
        }
        
        return resultMap;
	}
	
	private HashMap<Integer,GlobalRule> getAllMatchingGlobalRules(){
		
        //Retrieves the main rule container
        HashMap<Integer,GlobalRule> mainGlobalRuleMap = GameManager.getInstance().getGlobalRuleMap();
        
        //Creates the intermediate result hashMap
        HashMap<Integer,GlobalRule> resultMap = new HashMap<Integer,GlobalRule>();
        
        //Retrieves currentLevelEquation
        Equation currentLevelEquation = GameLevelManager.getInstance().getEquationOfRunningGameLevel();
        
        //Checks if the equation has been retrieved successfully
		if (currentLevelEquation == null) {
			System.err.println("Can't get all matching global rules. Failed to retrieve equation of current game level");
			return null;
		}
        
        //Tries every rules on this modelPath
        for (Map.Entry<Integer,GlobalRule> currentGlobalRule : mainGlobalRuleMap.entrySet()) {
        	
        	//If the current global rule is applicable, stocks it in intermediate result hashMap
        	
        	//case matching left part of the global rule
        	if( currentLevelEquation.isGlobalRuleValid(currentGlobalRule.getValue().getLeftEquation(), currentGlobalRule.getValue().getRightEquation()) )
        	{
        		resultMap.put(currentGlobalRule.getKey(), currentGlobalRule.getValue());
        	}
        	//case matching right part of the global rule
        	else if( currentLevelEquation.isGlobalRuleValid(currentGlobalRule.getValue().getRightEquation(), currentGlobalRule.getValue().getLeftEquation()) )
        	{
        		resultMap.put(currentGlobalRule.getKey(), currentGlobalRule.getValue());
        	}
        	//case matching swapped left part of the global rule
        	else if( currentLevelEquation.isGlobalRuleValid(currentGlobalRule.getValue().getLeftEquation().getSwappedInstance(), currentGlobalRule.getValue().getRightEquation().getSwappedInstance()) )
        	{
        		resultMap.put(currentGlobalRule.getKey(), currentGlobalRule.getValue());
        	}
        	//case matching swapped right part of the global rule
        	else if( currentLevelEquation.isGlobalRuleValid(currentGlobalRule.getValue().getRightEquation().getSwappedInstance(), currentGlobalRule.getValue().getLeftEquation().getSwappedInstance()) )
        	{
        		resultMap.put(currentGlobalRule.getKey(), currentGlobalRule.getValue());
        	}
        }
        
        return resultMap;
	}
	
	private boolean applyLocalRule(LocalRule ruleToApply, String modelPathWhereToApply) {
		
        //Retrieves currentLevelEquation
        Equation currentLevelEquation = GameLevelManager.getInstance().getEquationOfRunningGameLevel();
        
        //Checks if the equation has been retrieved successfully
		if (currentLevelEquation == null) {
			System.err.println("Can't apply the local rule. Failed to retrieve equation of current game level");
			return false;
		}
		
		Equation ruleEquation = ruleToApply.getEquation();
		Equation ruleSwappedEquation = ruleToApply.getEquation().getSwappedInstance();
        
        if (currentLevelEquation.applyLocalRule (modelPathWhereToApply, ruleEquation) == true) {
        	return true;
        }
        else if (currentLevelEquation.applyLocalRule (modelPathWhereToApply, ruleSwappedEquation) == true) {
        	return true;
        }
		else {
			return false;
		}
	}
	
	private boolean applyGlobalRule(GlobalRule ruleToApply) {
		
        //Retrieves currentLevelEquation
        Equation currentLevelEquation = GameLevelManager.getInstance().getEquationOfRunningGameLevel();
        
        //Checks if the equation has been retrieved successfully
		if (currentLevelEquation == null) {
			System.err.println("Can't apply the global rule. Failed to retrieve equation of current game level");
			return false;
		}
		
		Equation ruleLeftEquation = ruleToApply.getLeftEquation();
		Equation ruleRightEquation = ruleToApply.getRightEquation();
		Equation ruleLeftSwappedEquation = ruleToApply.getLeftEquation().getSwappedInstance();
		Equation ruleRightSwappedEquation = ruleToApply.getLeftEquation();
		
        
        if (currentLevelEquation.applyGlobalRule(ruleLeftEquation, ruleRightEquation) == true) {
        	return true;
        }
        else if (currentLevelEquation.applyGlobalRule(ruleRightEquation, ruleLeftEquation) == true) {
        	return true;
        }
        else if (currentLevelEquation.applyGlobalRule(ruleLeftSwappedEquation, ruleRightSwappedEquation) == true) {
        	return true;
        }
        else if (currentLevelEquation.applyGlobalRule(ruleRightSwappedEquation, ruleLeftSwappedEquation) == true) {
        	return true;
        }
        else {
        	return false;
        }
	}
	
	private boolean applyGlobalUnaryOperation(Equation currentLevelEquation, Operator operator) {
		
    	//Retrieves in the equation's left part the root expression
    	Expression exprToReplaceLeft = currentLevelEquation.search("0");
    	//Retrieves in the equation's right part the root expression
    	Expression exprToReplaceRight = currentLevelEquation.search("1");
    	
    	if (exprToReplaceLeft == null) {
    		System.err.println("Can't retrive possible operations. The left expression seems to be empty");
    		return false;
    	}
    	
    	if (exprToReplaceRight == null) {
    		System.err.println("Can't retrive possible operations. The right expression seems to be empty");
    		return false;
    	}
    	
    	//Copies the left expression to replace in order to restore it if the second replace fails (but the first replace succeed)
    	Expression exprToReplaceLeftCopy = exprToReplaceLeft.clone();

    	//Creates the application of unary operator on the left part of the equation (global application)
    	UnaryExpression exprToPlaceLeft = new UnaryExpression(exprToReplaceLeft, operator);
    	//Creates the application of unary operator on the right part of the equation (global application)
    	UnaryExpression exprToPlaceRight = new UnaryExpression(exprToReplaceRight, operator);
    	
    	//Proceeds the replace of the left part of the level equation
    	if (currentLevelEquation.replace("0", exprToPlaceLeft) == false) {
    		System.err.println("Can't retrive possible operations. The replace operation on the left expression failed");
    		return false;
    	}
    	
    	//Proceeds the replace of the right part of the level equation
    	if (currentLevelEquation.replace("1", exprToPlaceRight) == false) {
    		//Restores the initial LeftEquation
      		currentLevelEquation.replace("0", exprToReplaceLeftCopy);
    		System.err.println("Can't retrive possible operations. The replace operation on the right expression failed");
    		return false;
    	}
    	else {
    		return true;
    	}
	}
	
	private boolean applyGlobalBinaryOperation(Equation currentLevelEquation, Operator operator, TerminalExpression rightArgument) {
		
		//Retrieves in the equation's left part the root expression
    	Expression exprToReplaceLeft = currentLevelEquation.search("0");
    	//Retrieves in the equation's right part the root expression
    	Expression exprToReplaceRight = currentLevelEquation.search("1");
    	
    	if (exprToReplaceLeft == null) {
    		System.err.println("Can't retrive possible operations. The left expression seems to be empty");
    		return false;
    	}
    	
    	if (exprToReplaceRight == null) {
    		System.err.println("Can't retrive possible operations. The right expression seems to be empty");
    		return false;
    	}
    	
    	//Copies the left expression to replace in order to restore it if the second replace fails (but the first replace succeed)
    	Expression exprToReplaceLeftCopy = exprToReplaceLeft.clone();

    	//Creates the application of binary operator with the left part of the equation as left argument and the rightArgument variable as the right argument of the operator (global application)
    	BinaryExpression exprToPlaceLeft = new BinaryExpression(exprToReplaceLeft, rightArgument, operator);
    	//Creates the application of binary operator with the right part of the equation as left argument and the rightArgument variable as the right argument of the operator (global application)
    	BinaryExpression exprToPlaceRight = new BinaryExpression(exprToReplaceRight, rightArgument, operator);
    	
    	//Proceeds the replace of the left part of the level equation
    	if (currentLevelEquation.replace("0", exprToPlaceLeft) == false) {
    		System.err.println("Can't retrive possible operations. The replace operation on the left expression failed");
    		return false;
    	}
    	
    	//Proceeds the replace of the right part of the level equation
    	if (currentLevelEquation.replace("1", exprToPlaceRight) == false) {
    		//Restores the initial LeftEquation
      		currentLevelEquation.replace("0", exprToReplaceLeftCopy);
    		System.err.println("Can't retrive possible operations. The replace operation on the right expression failed");
    		return false;
    	}
    	else {
    		return true;
    	}
	}
	
	
	private boolean isAllowedByLevel(TerminalExpression pileElement) {
		
    	//Checks if the argument is corresponding to an element of the pile (allowed by this level)
    	ArrayList<TerminalExpression> levelPile = GameLevelManager.getInstance().getCurrentLevelPile();
    	
    	if (levelPile == null) {
    		System.err.println("Can't check if this pile element is allowed by level. No level pile is existing");
    		return false;
    	}
		
    	boolean isArgAllowed = false;
    	
    	for (TerminalExpression terminalExpressionIt : levelPile) {
    		if (pileElement.equals(terminalExpressionIt) == true) {
    			//the arg have been found in the container. It is allowed
    			isArgAllowed = true;
    			break;
    		}
    	}
    	
    	return isArgAllowed;
	}
	
	private boolean checkOperatorArguments(Operator operator, String[] operatorArgs) {
		
        //Checks argument number according to the operator type : unary must have 0 arg and binary must have 1 arg
        switch (operator.getArity()) {
        case 1:
        	if (operatorArgs == null) {
        		operatorArgs = new String[0];
        		return true;
        	}
        	else if (operatorArgs.length > 0) {
        		System.err.println("Can't retrive possible operations. Operator don't need argument and some argument have been given");
        		return false;
        	}
        	return true;
        case 2:
        	if ((operatorArgs == null) || (operatorArgs.length != 1)) {
        		System.err.println("Can't retrive possible operations. \"" + operator.getSymbol() + "\" operator need one argument. The number of given arguments does not match");
        		return false;
        	}
        	return true;
        default:
        	System.err.println("Can't retrive possible operations. The system currently only manage binary and unary expressions. Operator has been badly defined");
        	return false;
        }
	}
	
	private TerminalExpression retrieveTerminalExpressionFromArg(String arg) {

		TerminalExpression terminalExpression = null;
    	
    	//TODO check if we can avoid the use of try catch to do that
    	//if argument is a LiteralConstant
    	try {
    		int literalConstValue = Integer.parseInt(arg);
    		terminalExpression = new LiteralConstant(literalConstValue);
    	}
    	//if argument is a Variable
    	catch (NumberFormatException e) {
    		terminalExpression = new Variable(arg);
    	}
    	return terminalExpression;
	}
	
}
