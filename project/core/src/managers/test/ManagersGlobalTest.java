package managers.test;

import static org.junit.Assert.*;

import org.junit.Test;

import managers.GameLevelManager;
import managers.GameManager;
import managers.RulesApplicationManager;

/**
 * Test the utilization of the system .
 * 
 * @since 1.0
 * @see GameManager
 * @see GameLevelManager
 * @see RulesApplicationManager
 * 
 */

public class ManagersGlobalTest {

	static boolean isInit = false;
	
	public ManagersGlobalTest() {
		
		if(isInit == false) {
			// Program initialization
			if (GameManager.getInstance().init("../ressources_test/operatorsAndRulesTest.txt","../ressources_test/gameLevelsTest.txt") == false) {
				System.out.println("Failed to initialize the program");
				System.out.println("Program exit");
				System.exit(-1);
			}
			isInit = true;
		}
	}
	
	/**
	 * Tests the function that retrieves game level state.
	 */
	@Test
	public void getAllGameLevelsStringsTest() {
		
		String[][] gameLevels = GameManager.getInstance().getAllGameLevelsStrings();
		
		assertTrue (gameLevels != null);
		
		//only one level
		assertTrue (gameLevels.length == 1);
		//five fields by level
		assertTrue (gameLevels[0].length == 5);
		
		assertTrue (gameLevels[0][0].compareTo("1") == 0);
		assertTrue (gameLevels[0][1].compareTo("[(,x,+,a,),#,b]") == 0);
		assertTrue (gameLevels[0][2].compareTo("[0, 00, 0, 01, 0, 2, 1]") == 0);
		assertTrue (gameLevels[0][3].compareTo("[-]") == 0);
		assertTrue (gameLevels[0][4].compareTo("[a]") == 0);
	}

	/**
	 * Tests the function that retrieves possible form changes on a given model path.
	 */
	@Test
	public void getPossibleFormChangesTest() {
		
		int levelToLoad = 1;

		assertTrue (GameLevelManager.getInstance().loadGameLevelById(levelToLoad) == true);
		
		String modelPath = "0";
		
		String[][] possibleFormChanges = RulesApplicationManager.getInstance().getPossibleFormChanges(modelPath);
		
		assertTrue (possibleFormChanges != null);
		
		//nine possible form changes
		assertTrue (possibleFormChanges.length == 9);
		//two fields by possible form changes
		assertTrue (possibleFormChanges[0].length == 2);
		
		assertTrue (possibleFormChanges[0][0].compareTo("2") == 0);
		assertTrue (possibleFormChanges[0][1].compareTo("(a+0)#a") == 0);
		
		assertTrue (possibleFormChanges[1][0].compareTo("3") == 0);
		assertTrue (possibleFormChanges[1][1].compareTo("(0+a)#a") == 0);
		
		assertTrue (possibleFormChanges[2][0].compareTo("52") == 0);
		assertTrue (possibleFormChanges[2][1].compareTo("(a/1)#a") == 0);
		
		assertTrue (possibleFormChanges[3][0].compareTo("55") == 0);
		assertTrue (possibleFormChanges[3][1].compareTo("((!a)/(!1))#a") == 0);
		
		assertTrue (possibleFormChanges[4][0].compareTo("8") == 0);
		assertTrue (possibleFormChanges[4][1].compareTo("(!(!a))#a") == 0);
		
		assertTrue (possibleFormChanges[5][0].compareTo("11") == 0);
		assertTrue (possibleFormChanges[5][1].compareTo("(a-0)#a") == 0);
		
		assertTrue (possibleFormChanges[6][0].compareTo("28") == 0);
		assertTrue (possibleFormChanges[6][1].compareTo("(a*1)#a") == 0);
		
		assertTrue (possibleFormChanges[7][0].compareTo("29") == 0);
		assertTrue (possibleFormChanges[7][1].compareTo("(1*a)#a") == 0);
		
		assertTrue (possibleFormChanges[8][0].compareTo("14") == 0);
		assertTrue (possibleFormChanges[8][1].compareTo("(0-(!a))#a") == 0);
	}

	/**
	 * Tests the function that applies a local rule on a given model path.
	 */
	@Test
	public void applyLocalRuleTest() {
		
		int levelToLoad = 1;
		assertTrue (GameLevelManager.getInstance().loadGameLevelById(levelToLoad));
		
		String[] currentGameLevelStr = GameLevelManager.getInstance().getCurrentGameLevelStrings();
		
		assertTrue (currentGameLevelStr != null);
		
		assertTrue (currentGameLevelStr.length == 5);
		
		assertTrue (currentGameLevelStr[0].compareTo("1") == 0);
		assertTrue (currentGameLevelStr[1].compareTo("[(x+a)#b]") == 0);
		assertTrue (currentGameLevelStr[2].compareTo("[0, 00, 0, 01, 0, 2, 1]") == 0);
		assertTrue (currentGameLevelStr[3].compareTo("[-]") == 0);
		assertTrue (currentGameLevelStr[4].compareTo("[a]") == 0);
		
		
		assertTrue (RulesApplicationManager.getInstance().applyLocalRule(2, "0"));
		
		currentGameLevelStr = GameLevelManager.getInstance().getCurrentGameLevelStrings();
		
		assertTrue (currentGameLevelStr.length == 5);
		
		assertTrue (currentGameLevelStr[0].compareTo("1") == 0);
		assertTrue (currentGameLevelStr[1].compareTo("[((x+a)+0)#b]") == 0);
		assertTrue (currentGameLevelStr[2].compareTo("[0, 00, 000, 00, 001, 00, 0, 01, 0, 2, 1]") == 0);
		assertTrue (currentGameLevelStr[3].compareTo("[-]") == 0);
		assertTrue (currentGameLevelStr[4].compareTo("[a]") == 0);
	
	}

	/**
	 * Tests the function that retrieves possible global rules on the entire current level.
	 */
	@Test
	public void getPossibleGlobalFormChangesTest() {
		
		int levelToLoad = 1;

		assertTrue (GameLevelManager.getInstance().loadGameLevelById(levelToLoad) == true);
		
		String[][] possibleFormChanges = RulesApplicationManager.getInstance().getPossibleGlobalFormChanges();
		
		assertTrue (possibleFormChanges != null);
		
		//nine possible form changes
		assertTrue (possibleFormChanges.length == 2);
		//two fields by possible form changes
		assertTrue (possibleFormChanges[0].length == 2);
		
		assertTrue (possibleFormChanges[0][0].compareTo("59") == 0);
		assertTrue (possibleFormChanges[0][1].compareTo("(a+b)#c ## ((a+b)/d)#(c/d)") == 0);
		
		assertTrue (possibleFormChanges[1][0].compareTo("60") == 0);
		assertTrue (possibleFormChanges[1][1].compareTo("(a+b)#c ## ((a+b)*d)#(c*d)") == 0);
	
	}

	/**
	 * Tests the function that applies a global rule on the entire current level.
	 */
	@Test
	public void applyGlobalRuleTest() {
		
		int levelToLoad = 1;
		assertTrue (GameLevelManager.getInstance().loadGameLevelById(levelToLoad));
		
		String[] currentGameLevelStr = GameLevelManager.getInstance().getCurrentGameLevelStrings();
		
		assertTrue (currentGameLevelStr != null);
		
		assertTrue (currentGameLevelStr.length == 5);
		
		assertTrue (currentGameLevelStr[0].compareTo("1") == 0);
		assertTrue (currentGameLevelStr[1].compareTo("[(x+a)#b]") == 0);
		assertTrue (currentGameLevelStr[2].compareTo("[0, 00, 0, 01, 0, 2, 1]") == 0);
		assertTrue (currentGameLevelStr[3].compareTo("[-]") == 0);
		assertTrue (currentGameLevelStr[4].compareTo("[a]") == 0);
		
		
		assertTrue (RulesApplicationManager.getInstance().applyGlobalRule(59));
		
		currentGameLevelStr = GameLevelManager.getInstance().getCurrentGameLevelStrings();
		
		assertTrue (currentGameLevelStr.length == 5);
		
		assertTrue (currentGameLevelStr[0].compareTo("1") == 0);
		assertTrue (currentGameLevelStr[1].compareTo("[((x+a)/d)#(b/d)]") == 0);
		assertTrue (currentGameLevelStr[2].compareTo("[0, 00, 000, 00, 001, 00, 0, 01, 0, 2, 1, 10, 1, 11, 1]") == 0);
		assertTrue (currentGameLevelStr[3].compareTo("[-]") == 0);
		assertTrue (currentGameLevelStr[4].compareTo("[a]") == 0);
	
	}
	
	/**
	 * Tests the function that applies an operator (and its argument if needed) to the entire current level.
	 */
	@Test
	public void applyOperationTest() {
		
		int levelToLoad = 1;
		assertTrue (GameLevelManager.getInstance().loadGameLevelById(levelToLoad));
		
		String operatorTest = "-";
		int arity = GameManager.getInstance().getOperatorArity(operatorTest);
		
		String[] argTest = new String[arity-1];
		argTest[0] = "a";
		
		assertTrue (arity == 2);
		
		String[] currentGameLevelStr = GameLevelManager.getInstance().getCurrentGameLevelStrings();
		
		assertTrue (currentGameLevelStr != null);
		
		assertTrue (currentGameLevelStr.length == 5);
		
		assertTrue (currentGameLevelStr[0].compareTo("1") == 0);
		assertTrue (currentGameLevelStr[1].compareTo("[(x+a)#b]") == 0);
		assertTrue (currentGameLevelStr[2].compareTo("[0, 00, 0, 01, 0, 2, 1]") == 0);
		assertTrue (currentGameLevelStr[3].compareTo("[-]") == 0);
		assertTrue (currentGameLevelStr[4].compareTo("[a]") == 0);
		
		
		assertTrue (RulesApplicationManager.getInstance().applyOperation(operatorTest, argTest));
		
		
		currentGameLevelStr = GameLevelManager.getInstance().getCurrentGameLevelStrings();
		
		assertTrue (currentGameLevelStr != null);
		
		assertTrue (currentGameLevelStr.length == 5);
		
		assertTrue (currentGameLevelStr[0].compareTo("1") == 0);
		assertTrue (currentGameLevelStr[1].compareTo("[((x+a)-a)#(b-a)]") == 0);
		assertTrue (currentGameLevelStr[2].compareTo("[0, 00, 000, 00, 001, 00, 0, 01, 0, 2, 1, 10, 1, 11, 1]") == 0);
		assertTrue (currentGameLevelStr[3].compareTo("[-]") == 0);
		assertTrue (currentGameLevelStr[4].compareTo("[a]") == 0);
	
	}

}
