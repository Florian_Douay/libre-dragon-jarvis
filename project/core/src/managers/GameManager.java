package managers;

import java.util.ArrayList;
import java.util.HashMap;

import modele.metier.GameLevel;
import modele.metier.GlobalRule;
import modele.metier.Operator;
import modele.metier.LocalRule;
import modele.technique.ModelImplementer;
import modele.technique.parser.IParserResult;
import modele.technique.parser.Parser;
import modele.technique.parser.ParserResult;

/**
 * Manager that initializes the system and that contains model instances
 * <p>
 * It allows :
 * <ul>
 * <li>To initializes the system with given configuration files paths</li>
 * <li>To get every model instances containers</li>
 * <li>To get some specific model informations </li>
 * </ul>
 * 
 * @since 1.0
 * @see GameLevel
 * @see Operator
 * @see LocalRule
 * @see GlobalRule
 * @see Operator
 * 
 */

public class GameManager {

	private static GameManager INSTANCE = new GameManager();
	
	boolean isInitialized;
	private ArrayList<GameLevel> levelArray;
	private HashMap<String,Operator> operatorMap;
	private HashMap<Integer,LocalRule> localRuleMap;
	private HashMap<Integer,GlobalRule> globalRuleMap;
	
	/**
	 * Returns the unique instance of the class.
	 * @return the unique instance of the class.
	 */
	public static GameManager getInstance () {
		return INSTANCE;
	}
	
	/**
	 * Private default constructor.
	 * Is only used on initializing the static instance of the class.
	 */
	private GameManager() {
		this.isInitialized = false;
		this.levelArray = new ArrayList<GameLevel>();
		this.operatorMap = new HashMap<String,Operator>();
		this.localRuleMap = new HashMap<Integer,LocalRule>();
		this.globalRuleMap = new HashMap<Integer,GlobalRule>();
	}
	
	/**
	 * Initializes the system by parsing configuration files and implementing corresponding model.
	 * @param pathToOperatorConfigFile the path to the operators and rules configuration file.
	 * @param pathToLevelsConfigFile the path to the game levels configuration file.
	 * @return true if the system has been initialized successfully. false otherwise.
	 */
	public boolean init(String pathToOperatorConfigFile, String pathToLevelsConfigFile) {
		
		// ********** Testing if already initialized *************
		
		if (isInitialized) {
			System.err.println("Can't initialize GameManager. It has been already initialized");
			return false;
		}
		
		// ********** Parsing *************
		
		//Instantiate the main parser
		Parser parser = new Parser(pathToOperatorConfigFile, pathToLevelsConfigFile);
		
		if (parser.parseAll() == false) {
			System.err.println("Parsing of config files failed");
			return false;
		}
		
		// Retrieve the parsing result
		IParserResult iparserResult = parser.getParserResult();
		
		// Explicit the fact that the kind of IParserResult is a ParserResult
		ParserResult parserResult = (ParserResult) iparserResult;
		
		// ********** Model implementation *************
		
		// Instantiate a model implementer
		ModelImplementer modeleImplementer = new ModelImplementer(parserResult);
		
		// Implement the model
		operatorMap.putAll(modeleImplementer.getOperatorMap());
		localRuleMap.putAll(modeleImplementer.getLocalRuleArray());
		globalRuleMap.putAll(modeleImplementer.getGlobalRuleArray());
		levelArray.addAll(modeleImplementer.getGameLevelArray());
		
		
		// ********** Ending *************
		
		isInitialized = true;
		
		return true;
	}
	
	/**
	 * Returns the game level container.
	 * @return the game level container.
	 */
	public final ArrayList<GameLevel> getGameLevelArray() {
		return this.levelArray;
	}
	
	/**
	 * Returns the operator container.
	 * @return the operator container.
	 */
	public final HashMap<String,Operator> getOperatorMap() {
		return this.operatorMap;
	}
	
	/**
	 * Returns the local rule container.
	 * @return the local rule container.
	 */
	public final HashMap<Integer,LocalRule> getLocalRuleMap() {
		return this.localRuleMap;
	}
	
	/**
	 * Returns the global rule container.
	 * @return the global rule container.
	 */
	public final HashMap<Integer,GlobalRule> getGlobalRuleMap() {
		return this.globalRuleMap;
	}
	
	/**
	 * Returns the GameLevel corresponding to the given id.
	 * @param id the id of the level to load.
	 * @return a GameLevel if the id is correct. null otherwise.
	 */
	protected final GameLevel getGameLevelById(int id) {
		
		if (levelArray.size() == 0) {
			System.err.println("Can't get gameLevel by id. No level is currently existing");
			return null;
		}
		
		for (GameLevel gameLevel : levelArray) {
	        if (gameLevel.getLevelId() == id) {
	        	return gameLevel;
	        }
		}
		
		System.err.println("Unable to get GameLevel by id. The id searched don't exists");
		return null;
	}
	
	/**
	 * Returns an array of array of strings representing all game levels.
	 * @return an array of array of strings.
	 */
	public String[][] getAllGameLevelsStrings() {
		
		int numberOfGameLevels = levelArray.size();
		
		if (numberOfGameLevels == 0) {
			System.err.println("Can't get gameLevels as string[][]. No level is currently existing");
			return null;
		}
		
		//TODO check if can do this smarter
		int numberOfStringsForAGameLevel = 5;
		
		String[][] gameLevelsStrings = new String [numberOfGameLevels][numberOfStringsForAGameLevel];
		
		int i = 0;
		for (GameLevel gameLevel : levelArray) {
			gameLevelsStrings[i++] = gameLevel.toStringsWithSeparators();
		}
		
		return gameLevelsStrings;
	}
	
	/**
	 * Returns the image path of operator corresponding to the given operator symbol.
	 * @param operatorSymbol the symbol of operator to get the image path.
	 * @return a string if operator exists. null otherwise.
	 */
	public String getOperatorImgPathFromSymbol(String operatorSymbol) {
		
		Operator operator = operatorMap.get(operatorSymbol);
		
		if (operator == null) {
			System.err.println("Can't get the path to the img file corresponding to symbol \'" + operatorSymbol + "\'. This symbol is not reconized.");
			return null;
		}
		
		return operator.getImgPath();
	}

	/**
	 * Returns the arity of the symbol corresponding to the given operator symbol.
	 * @param operatorSymbol the symbol of operator to get the arity.
	 * @return an integer if operator exists. -1 otherwise.
	 */
	public int getOperatorArity(String operatorSymbol) {
		
		Operator operator = operatorMap.get(operatorSymbol);
		
		if (operator == null) {
			System.err.println("Can't get the arity of the operator \'" + operatorSymbol + " \'. This operator seems to do not exist.");
			return -1;
		}
		
		return operator.getArity();
	}
	
}
