package managers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Observer;

import modele.metier.Equation;
import modele.metier.GameLevel;
import modele.metier.Operator;
import modele.metier.RunningGameLevel;
import modele.metier.TerminalExpression;

/**
 * Manager that allows to manage current game level
 * <p>
 * It allows :
 * <ul>
 * <li>To indicate if the current game level has been won</li>
 * <li>To restart the current game level</li>
 * <li>To retrieve the state of the current game level as a tab of strings</li>
 * <li>To do loading and unloading operations of level</li>
 * <li>To add/remove state observers of game levels</li>
 * </ul>
 * 
 * @since 1.0
 * @see RunningGameLevel
 * @see GameLevel
 * 
 */

public class GameLevelManager {
	
	private static GameLevelManager INSTANCE = new GameLevelManager();
	
	private RunningGameLevel runningGameLevel;
	private ArrayList<Observer> observersArray;
	
	/**
	 * Returns the unique instance of the class.
	 * @return the unique instance of the class.
	 */
	public static GameLevelManager getInstance () {
		return INSTANCE;
	}
	
	/**
	 * Private default constructor.
	 * Is only used on initializing the static instance of the class.
	 */
	private GameLevelManager() {
		
		runningGameLevel = null;
		observersArray = new ArrayList<Observer>();
	}
	
	/**
	 * Returns if a game level is currently loaded.
	 * @return true if a game level is currently loaded. false otherwise.
	 */
	public boolean isAGameLevelLoaded() {

		return (runningGameLevel != null);
	}
	
	/**
	 * Returns if the current game level has been won.
	 * @return true if there is a game level loaded and it has been won. false otherwise.
	 */
	public boolean isTheCurrentGameLevelWon() {
		
		if (runningGameLevel == null) {
			System.err.println("Can't know if the gameLevel is won. No level is currently loaded");
			return false;
		}
		
		return runningGameLevel.isLevelWon();
	}
	
	/**
	 * Returns an array of strings representing the current state of the current game level.
	 * @return a string array if a level is currently loaded. null otherwise.
	 */
	public String[] getCurrentGameLevelStrings() {
		
		if (runningGameLevel == null) {
			System.err.println("Can't get the gameLevel as string[]. No level is currently loaded");
			return new String[0];
		}

		return runningGameLevel.toStrings();
	}
	
	/**
	 * Returns an array of strings representing the current state of the current game level.
	 * @return a string array if a level is currently loaded. null otherwise.
	 */
	public String[] getCurrentGameLevelStringsWithSeparators() {
		
		if (runningGameLevel == null) {
			System.err.println("Can't get the gameLevel as string[] with separators. No level is currently loaded");
			return new String[0];
		}

		return runningGameLevel.toStringsWithSeparators();
	}

	/**
	 * Loads as current game level the level corresponding to the given id.
	 * @param id the id of the game level to load.
	 * @return true if the level has been loaded successfully. false otherwise.
	 */
	public boolean loadGameLevelById(int id) {
		
		GameLevel gameLevel = GameManager.getInstance().getGameLevelById(id);
		
		if (gameLevel == null) {
			System.err.println("Can't load level with id:" + id + ". Unable to retrieve the gameLevel");
			return false;
		}
		
		runningGameLevel = new RunningGameLevel(gameLevel);
		
		if (runningGameLevel == null) {
			System.err.println("Can't load gameLevel. The instantiation didn't work");
			return false;
		}
		
		subscribeAllObserversToLevel();
		notifySubscribedObservers();
		return true;
	}
	
	/**
	 * Unloads the current game level.
	 * @return true if the current game level has been unloaded successfully. false otherwise.
	 */
	public boolean unloadCurrentGameLevel() {
		
		if (runningGameLevel == null) {
			System.err.println("Can't unload gameLevel. No level is currently loaded");
			return false;
		}
		else {
			runningGameLevel.deleteObservers();
			runningGameLevel = null;
			return true;
		}
	}
	
	/**
	 * Restarts the current game level.
	 * @return true if the current game level has been restarted successfully. false otherwise.
	 */
	public boolean restartCurrentGameLevel() {
		
		if (runningGameLevel == null) {
			System.err.println("Can't restart gameLevel. No level is currently loaded");
			return false;
		}
		
		int levelId = runningGameLevel.getLevelId();
		
		GameLevel gameLevelToReload = GameManager.getInstance().getGameLevelById(levelId);
		
		if (gameLevelToReload == null) {
			System.err.println("Can't restart gameLevel. Unable to retrieve the gameLevel");
			return false;
		}
		else {
			runningGameLevel = new RunningGameLevel(gameLevelToReload);
			subscribeAllObserversToLevel();
			notifySubscribedObservers();
			return true;
		}
	}

	/**
	 * Adds the given observer in observers list. Each change in the model of current and future current levels will notify this observer. 
	 * @param observer the observer to add.
	 * @return true if the operation succeed. false if error or already existing.
	 */
	public boolean addObserverToLevelsModel(Observer observer) {
		
		if (observer == null) {
			System.err.println("Can't add this observer to the Level. The given argument is null reference");
			return false;	
		}
		
		if (observersArray.lastIndexOf(observer) != -1) {
			System.err.println("Can't add this observer to the Level. It has been already added");
			return false;
		}
		
		if (observersArray.add(observer) == false) {
			System.err.println("Failed to add this observer to the Level. Add function failed");
			return false;
		}
		
		if (runningGameLevel != null) {
			runningGameLevel.addObserver(observer);
		}
		return true;
	}

	/**
	 * Removes the given observer of the observers list. This observer won't be notified of model changes anymore.
	 * @param observer the observer to remove.
	 * @return true if the operation succeed. false if error or element not existing.
	 */
	public boolean removeObserverFromLevelsModel(Observer observer) {
		
		if (observer == null) {
			System.err.println("Can't remove this observer of the Level. The given argument is null reference");
			return false;	
		}
		
		if (observersArray.lastIndexOf(observer) == -1) {
			System.err.println("Can't remove this observer of the Level. It has'nt been added yet");
			return false;
		}
		
		int observerIndex;
		
		while ( (observerIndex=observersArray.lastIndexOf(observer)) != -1) {
			observersArray.remove(observerIndex);
		}
		
		if (runningGameLevel != null) {
			runningGameLevel.deleteObserver(observer);
		}
		
		return true;
	}

	/**
	 * Removes all observers of the observers list. No observer will be notified of model changes anymore.
	 */
	public void removeAllObserversFromLevelsModel() {
		
		observersArray.clear();
		runningGameLevel.deleteObservers();
	}
	
	/**
	 * Adds in the current level's observers list all observers that has been registered for watching level's model.
	 */
	private void subscribeAllObserversToLevel() {
		
		if (runningGameLevel == null) {
			System.err.println("Can't subscribe observers to the running level. No level is currently loaded");
			return;
		}
		
		for (Observer observer : observersArray) {
			if (observer != null) {
				runningGameLevel.addObserver(observer);
			}
		}
	}
	
	/**
	 * Notifies all current game level's observers that model has changed.
	 */
	protected void notifySubscribedObservers() {
		if (runningGameLevel == null) {
			System.err.println("Can't notify observers of the running level. No level is currently loaded");
			return;
		}
		
		runningGameLevel.setModelChanged();
		runningGameLevel.notifyObservers();
	}

	/**
	 * Returns if the given model path is existing in the current game level.
	 * @param modelPath the model path to check the existence.
	 * @return true if a level is loaded and model path exists in the current game level. false otherwise.
	 */
	protected boolean isPathExistingInModel(String modelPath) {
		if (runningGameLevel == null) {
			System.err.println("Can't determine if the given modelPath exists. No level is currently loaded");
			return false;
		}
		return runningGameLevel.isPathExistingInModel(modelPath);
	}

	/**
	 * Returns the pile container of the current game level.
	 * @return the pile container of the current game level. null if no level is loaded.
	 */
	protected final ArrayList<TerminalExpression> getCurrentLevelPile() {
		
		if (runningGameLevel == null) {
			System.err.println("Can't get the the running level's pile. No level is currently loaded");
			return null;
		}
		return runningGameLevel.getLevelPile();
	}

	/**
	 * Returns the operator container of the current game level.
	 * @return the operator container of the current game level. null if no level is loaded.
	 */
	protected final HashMap<String,Operator> getCurrentLevelOperators() {
		
		if (runningGameLevel == null) {
			System.err.println("Can't get the the running level's operators. No level is currently loaded");
			return null;
		}
		return runningGameLevel.getLevelOperators();
	}
	
	/**
	 * Returns the Equation of the current game level.
	 * @return the Equation of the current game level. null if no level is loaded.
	 */
	protected Equation getEquationOfRunningGameLevel() {
		
		if (runningGameLevel == null) {
			System.err.println("Can't retrives the current game level's equation. No level is currently loaded");
			return null;
		}
		
		return runningGameLevel.getEquation();
	}

}
