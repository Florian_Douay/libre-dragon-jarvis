package modele.metier;

/**
 * NonTerminalExpression that defines an Expression with one Operator and one Expression.
 * 
 * @since 1.0
 * @see modele.metier.Expression
 * @see modele.metier.NonTerminalExpression
 * 
 */

public class UnaryExpression extends NonTerminalExpression {

	/**
	 * Expression of the UnaryExpression.
	 */
	private Expression expression;
	/**
	 * Operator of the BinaryExpression.
	 */
	private Operator operator;

	/**
	 * Constructor UnaryExpression.
	 * @param expression Expression of the BinaryExpression.
	 * @param operator the operator of the BinaryExpression.
	 */
	public UnaryExpression (Expression expression, Operator operator) {
		super(ExpressionType.UNARY_EXPRESSION);
		this.expression = expression;
		this.operator = operator;
	}
	
	/**
	 * Copy constructor of UnaryExpression.
	 * @param unaryBinary UnaryExpression copied to create the new UnaryExpression.
	 */
	public UnaryExpression(UnaryExpression unaryBinary) {
		super(ExpressionType.UNARY_EXPRESSION);
		this.expression = unaryBinary.expression.clone();
		this.operator = new Operator(unaryBinary.operator);
	}
	
	/**
	 * Getter of the Expression of the UnaryExpression.
	 * @return Expression of the UnaryExpression.
	 */
	public Expression getExpression() {
		return expression;
	}

	/**
	 * Getter of the operator of the UnaryExpression.
	 * @return the operator of the UnaryExpression.
	 */		
	public Operator getOperator() {
		return operator;
	}

	/**
	 * Setter of the Expression.
	 * @param expression the Expression that replaces the existing Expression.
	 */
	public void setExpression(Expression expression) {
		this.expression = expression;
	}
	
	/* (non-Javadoc)
	 * @see modele.metier.Expression#toStringWithSeparator()
	 */
	@Override
	public String toStringWithSeparator() {
		return "(" + "," + operator.toString() + "," + expression.toStringWithSeparator() + "," + ")";
	}

	/* (non-Javadoc)
	 * @see modele.metier.NonTerminalExpression#toString()
	 */
	@Override
	public String toString() {
		//TODO check if we need to implement the two cases of UnaryOperators : OP EXP and EXP OP
		return "(" + operator.toString() + expression.toString() + ")";
	}
	
	/* (non-Javadoc)
	 * @see modele.metier.NonTerminalExpression#clone()
	 */
	@Override
	public UnaryExpression clone() {
		return new UnaryExpression(this);
	}

	/* (non-Javadoc)
	 * @see modele.metier.NonTerminalExpression#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((expression == null) ? 0 : expression.hashCode());
		result = prime * result + ((operator == null) ? 0 : operator.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see modele.metier.NonTerminalExpression#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UnaryExpression other = (UnaryExpression) obj;
		if (expression == null) {
			if (other.expression != null)
				return false;
		} else if (!expression.equals(other.expression))
			return false;
		if (operator == null) {
			if (other.operator != null)
				return false;
		} else if (!operator.equals(other.operator))
			return false;
		return true;
	}
}
