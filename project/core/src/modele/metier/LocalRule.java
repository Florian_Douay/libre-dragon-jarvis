package modele.metier;

/**
 * Model class that defines a LocalRule with an Equation.
 * Used to transform an Expression (stored in an Equation) in a new one.
 * 
 * @since 1.0
 * @see modele.metier.Rule
 * @see modele.metier.Equation
 * 
 */

public class LocalRule extends Rule {
	
	/**
	 * Equation of the LocalRule.
	 */
	private Equation equation;
	
	/**
	 * Create an instance of LocalRule by using an Equation.
	 * @param equation Equation of the LocalRule.
	 */
	public LocalRule (Equation equation) {
		this.equation = equation;

	}

	/**
	 * Getter of the Equation of the LocalRule.
	 * @return the Equation of the LocalRule.
	 */
	public final Equation getEquation() {
		return equation;
	}
	
	/* (non-Javadoc)
	 * @see modele.metier.Rule#toString()
	 */
	@Override
	public String toString() {
		return equation.toString();
	}
	
}
