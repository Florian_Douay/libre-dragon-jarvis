package modele.metier;

/**
 * Enumeration that permits to determine the type of the Expressions stored.
 * 
 * @since 1.0
 * @see Expression
 * 
 */

public enum ExpressionType {
	LITERAL_CONSTANT,
	VARIABLE,
	UNARY_EXPRESSION,
	BINARY_EXPRESSION
}
