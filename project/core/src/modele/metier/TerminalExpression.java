package modele.metier;

/**
 * Expressions sees as leaf in an Equation tree.
 * 
 * @since 1.0
 * @see modele.metier.Expression
 * 
 */
public abstract class TerminalExpression extends Expression {
	
	/**
	 * Create an instance of TerminalExpression by using the ExpressionType.
	 * @param type ExpressionType of the TerminalExpression.
	 */
	public TerminalExpression(ExpressionType type) {
		super(type);
	}
	
	/* (non-Javadoc)
	 * @see modele.metier.Expression#toString()
	 */
	@Override
	public abstract String toString();
	
	/* (non-Javadoc)
	 * @see modele.metier.Expression#clone()
	 */
	@Override
	public abstract Expression clone();
	
	/* (non-Javadoc)
	 * @see modele.metier.Expression#hashCode()
	 */
	@Override
	public abstract int hashCode();

	/* (non-Javadoc)
	 * @see modele.metier.Expression#equals(java.lang.Object)
	 */
	@Override
	public abstract boolean equals(Object obj);
}
