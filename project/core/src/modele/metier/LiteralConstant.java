package modele.metier;

/**
 * TerminalExpression used to store constant.
 * 
 * @since 1.0
 * @see modele.metier.Expression
 * @see modele.metier.TerminalExpression
 * 
 */

public class LiteralConstant extends TerminalExpression {

	/**
	 * Value of the LiteralConstant.
	 */
	private int value;

	/**
	 * Create an instance of LiteralConstant by using integer (value).
	 * @param value Value of the LiteralConstant.
	 */
	public LiteralConstant (int value) {
		super(ExpressionType.LITERAL_CONSTANT);
		this.value = value;
	}

	/**
	 * Copy constructor of LiteralConstant.
	 * @param literalConstant LiteralConstant copied to create the new LiteralConstant.
	 */
	public LiteralConstant(LiteralConstant literalConstant) {
		super(ExpressionType.LITERAL_CONSTANT);
		this.value = new Integer(literalConstant.value);
	}

	/**
	 * Getter of the value.
	 * @return Value of the LiteralConstant.
	 */
	public int getValue() {
		return value;
	}
	
	/* (non-Javadoc)
	 * @see modele.metier.Expression#toStringWithSeparator()
	 */
	@Override
	public String toStringWithSeparator() {
		return toString();
	}
	
	/* (non-Javadoc)
	 * @see modele.metier.TerminalExpression#toString()
	 */
	@Override
	public String toString() {
		return Integer.toString(value);
	}

	/* (non-Javadoc)
	 * @see modele.metier.TerminalExpression#clone()
	 */
	@Override
	public LiteralConstant clone() {
		return new LiteralConstant(this);
	}
	
	/* (non-Javadoc)
	 * @see modele.metier.TerminalExpression#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + value;
		return result;
	}

	/* (non-Javadoc)
	 * @see modele.metier.TerminalExpression#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LiteralConstant other = (LiteralConstant) obj;
		if (value != other.value)
			return false;
		return true;
	}
}
