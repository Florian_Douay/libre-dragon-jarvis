package modele.metier.test;

import static org.junit.Assert.*;

import org.junit.Test;

import modele.metier.BinaryExpression;
import modele.metier.LiteralConstant;
import modele.metier.Operator;
import modele.metier.UnaryExpression;
import modele.metier.Variable;

/**
 * Test the functions of the UnaryExpression Class.
 * 
 * @since 1.0
 * @see modele.metier.UnaryExpression
 * 
 */

public class UnaryExpressionTest {

	/**
	 * Test the equals function of the UnaryExpression Class.
	 * @see modele.metier.UnaryExpression#equals()
	 */
	@Test
	public void testEquals() {
		Variable varB1 = new Variable("b");
		Variable varB2 = new Variable("b");
		Variable varC = new Variable("c");

		LiteralConstant cstTwo1 = new LiteralConstant(2);
		LiteralConstant cstTwo2 = new LiteralConstant(2); 
		LiteralConstant cstThree = new LiteralConstant(3);

		Operator plus = new Operator("plus", "+", "#", 2);
		Operator fois = new Operator("fois", "*", "#", 2);
		Operator moins = new Operator("moins", "-", "#", 1);
		Operator camion = new Operator("camion", "$", "#", 1);

		UnaryExpression expUn1 = new UnaryExpression(cstThree, camion);
		UnaryExpression expUn2 = new UnaryExpression(cstThree, camion);

		BinaryExpression expFois1 = new BinaryExpression(varB1, varC, fois);
		BinaryExpression expFois2 = new BinaryExpression(varB2, varC, fois);
		BinaryExpression expPlus1 = new BinaryExpression(expFois1, cstTwo1, plus);
		BinaryExpression expPlus2 = new BinaryExpression(expFois2, cstTwo2, plus);
		BinaryExpression expPlus3 = new BinaryExpression(expPlus1, expUn1, plus);
		BinaryExpression expPlus4 = new BinaryExpression(expPlus2, expUn2, plus);

		UnaryExpression expFinal1 = new UnaryExpression(expPlus3, moins);
		UnaryExpression expFinal2 = new UnaryExpression(expPlus4, moins);
		
		assertTrue(expUn1.equals(expUn2));
		assertTrue(expFinal1.equals(expFinal2));
		assertFalse(expFinal1.equals(expUn1));
	}
}
