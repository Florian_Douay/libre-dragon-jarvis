package modele.metier.test;

import static org.junit.Assert.*;

import org.junit.Test;

import modele.metier.BinaryExpression;
import modele.metier.Equation;
import modele.metier.LiteralConstant;
import modele.metier.Operator;
import modele.metier.UnaryExpression;
import modele.metier.Variable;

/**
 * Test the functions of the Equation Class.
 * 
 * @since 1.0
 * @see modele.metier.Equation
 * 
 */
public class EquationTest {

	/**
	 * Test the search function of the Equation Class.
	 * @see modele.metier.Equation#search()
	 */
	@Test
	public void testSearch() {
		Variable a = new Variable("a");
		Variable b = new Variable("b");
		Variable c = new Variable("c");
		Operator plus = new Operator("plus", "+", "#", 2);
		Operator fois = new Operator("fois", "*", "#", 2);
	
		BinaryExpression expLeftFois = new BinaryExpression(a, b, fois);
		BinaryExpression expRightPlus = new BinaryExpression(expLeftFois, c, plus);
		Equation eq1 = new Equation(expLeftFois, expRightPlus);

		assertTrue(eq1.search("11").toString().equals("c"));
	}

	/**
	 * Test the replace function of the Equation Class.
	 * @see modele.metier.Equation#replace()
	 */
	@Test
	public void testReplace() {
		Variable a = new Variable("a");
		Variable b = new Variable("b");
		Variable c = new Variable("c");
		Variable d = new Variable("d");
		Operator plus = new Operator("plus", "+", "#", 2);
		Operator fois = new Operator("fois", "*", "#", 2);
	
		BinaryExpression expLeftFois = new BinaryExpression(a, b, fois);
		BinaryExpression expRightPlus = new BinaryExpression(expLeftFois, c, plus);
		BinaryExpression expReplace = new BinaryExpression(d, c, plus);
		Equation eq1 = new Equation(expLeftFois, expRightPlus);

		eq1.replace("00", expReplace);
		assertTrue(eq1.toString().equals("((d+c)*b)#((a*b)+c)"));
	}

	/**
	 * Test the getTreePathIDs function of the Equation Class.
	 * @see modele.metier.Equation#getTreePathIDs()
	 */
	@Test
	public void testGetTreePathIDs() {
		Variable a = new Variable("a");
		Variable b = new Variable("b");
		Variable c = new Variable("c");
		Variable d = new Variable("d");
		Variable w = new Variable("w");

		Operator plus = new Operator("plus", "+", "#", 2);
		Operator fois = new Operator("fois", "*", "#", 2);
		Operator camion = new Operator("camion", "$", "#", 1);

		UnaryExpression expUn = new UnaryExpression(b, camion);

		BinaryExpression expLeftFois1 = new BinaryExpression(a, c, fois);
		BinaryExpression expRightFois1 = new BinaryExpression(expUn, d, fois);
		BinaryExpression expRightFois2 = new BinaryExpression(expUn, c, fois);
		
		BinaryExpression expLeftPlus = new BinaryExpression(expLeftFois1, expRightFois2, plus);
		BinaryExpression expLeftFois = new BinaryExpression(expLeftPlus, w, fois);
		BinaryExpression expRightPlus = new BinaryExpression(expRightFois1, expRightFois1, plus);

		Equation eq1 = new Equation(expLeftFois, expRightPlus);

		assertTrue(eq1.getTreePathIDs().size() == eq1.toString().length());
		assertTrue(eq1.getTreePathIDs().toString().equals("[0, 00, 000, 0000, 000, 0001, 000, 00, 001, 0010, 0010, 00100, 0010, 001, 0011, 001, 00, 0, 01, 0, 2, 1, 10, 100, 100, 1000, 100, 10, 101, 10, 1, 11, 110, 110, 1100, 110, 11, 111, 11, 1]"));
	}

	/**
	 * Test the getVarPathIDs function of the Equation Class.
	 * @see modele.metier.Equation#getVarPathIDs()
	 */
	@Test
	public void testGetVarPathIDs() {
		Variable a = new Variable("a");
		Variable b = new Variable("b");
		Variable c = new Variable("c");
		Variable d = new Variable("d");
		Operator plus = new Operator("plus", "+", "#", 2);
		Operator fois = new Operator("fois", "*", "#", 2);
	
		BinaryExpression expLeftFois = new BinaryExpression(a, b, fois);
		BinaryExpression expRightPlus = new BinaryExpression(expLeftFois, c, plus);
		BinaryExpression expReplace = new BinaryExpression(d, c, plus);
		Equation eq1 = new Equation(expLeftFois, expRightPlus);

		assertTrue(eq1.getVarPathIDs().get(a).toString().equals("[00, 100]"));
		assertTrue(eq1.getVarPathIDs().get(b).toString().equals("[01, 101]"));
		assertTrue(eq1.getVarPathIDs().get(c).toString().equals("[11]"));

		eq1.replace("00", expReplace);
		assertTrue(eq1.getVarPathIDs().get(a).toString().equals("[100]"));
		assertTrue(eq1.getVarPathIDs().get(b).toString().equals("[01, 101]"));
		assertTrue(eq1.getVarPathIDs().get(c).toString().equals("[001, 11]"));
		assertTrue(eq1.getVarPathIDs().get(d).toString().equals("[000]"));
	}

	/**
	 * Test the isLocalRuleValid function of the Equation Class.
	 * @see modele.metier.Equation#isLocalRuleValid()
	 */
	@Test
	public void testIsLocalRuleValid() {
		Variable a = new Variable("a");
		Variable b = new Variable("b");
		Variable c = new Variable("c");
		Variable d = new Variable("d");
		Variable w = new Variable("w");

		LiteralConstant deux = new LiteralConstant(2); 

		Operator plus = new Operator("plus", "+", "#", 2);
		Operator fois = new Operator("fois", "*", "#", 2);

		BinaryExpression expLeftFois1 = new BinaryExpression(a, c, fois);
		BinaryExpression expLeftFois2 = new BinaryExpression(a, a, plus);
		BinaryExpression expLeftFois3 = new BinaryExpression(deux, a, fois);
		BinaryExpression expRightFois1 = new BinaryExpression(b, d, fois);
		BinaryExpression expRightFois2 = new BinaryExpression(b, c, fois);
		BinaryExpression expLeftPlus = new BinaryExpression(expLeftFois1, expRightFois2, plus);
		BinaryExpression expLeftFois = new BinaryExpression(expLeftPlus, w, fois);
		BinaryExpression expRightPlus = new BinaryExpression(expRightFois1, expRightFois1, plus);
		BinaryExpression expLeftPlus1 = new BinaryExpression(a, b, plus);
		BinaryExpression expLeftPlus2 = new BinaryExpression(b, a, plus);

		Equation eq1 = new Equation(expLeftFois, expRightPlus);
		Equation rule1 = new Equation(expLeftPlus1, expLeftPlus2);
		Equation rule2 = new Equation(expLeftFois2, expLeftFois3);
		
		assertTrue(eq1.isLocalRuleValid("00", rule1));
		
		assertTrue(eq1.isLocalRuleValid("00", rule1.getSwappedInstance()));

		assertTrue(eq1.isLocalRuleValid("1", rule2));

		assertFalse(eq1.isLocalRuleValid("1", rule2.getSwappedInstance()));
	}

	/**
	 * Test the applyLocalRule function of the Equation Class.
	 * @see modele.metier.Equation#applyLocalRule()
	 */
	@Test
	public void testApplyLocalRule() {
		Variable a = new Variable("a");
		Variable b = new Variable("b");
		Variable c = new Variable("c");
		Variable d = new Variable("d");
		Variable w = new Variable("w");

		LiteralConstant deux = new LiteralConstant(2); 

		Operator plus = new Operator("plus", "+", "#", 2);
		Operator fois = new Operator("fois", "*", "#", 2);

		BinaryExpression expLeftFois1 = new BinaryExpression(a, c, fois);
		BinaryExpression expLeftFois2 = new BinaryExpression(a, a, plus);
		BinaryExpression expLeftFois3 = new BinaryExpression(deux, a, fois);
		BinaryExpression expRightFois1 = new BinaryExpression(b, d, fois);
		BinaryExpression expRightFois2 = new BinaryExpression(b, c, fois);
		BinaryExpression expLeftPlus = new BinaryExpression(expLeftFois1, expRightFois2, plus);
		BinaryExpression expLeftFois = new BinaryExpression(expLeftPlus, w, fois);
		BinaryExpression expRightPlus = new BinaryExpression(expRightFois1, expRightFois1, plus);
		BinaryExpression expLeftPlus1 = new BinaryExpression(a, b, plus);
		BinaryExpression expLeftPlus2 = new BinaryExpression(b, a, plus);

		Equation eq1 = new Equation(expLeftFois, expRightPlus);
		Equation rule1 = new Equation(expLeftPlus1, expLeftPlus2);
		Equation rule2 = new Equation(expLeftFois2, expLeftFois3);
		
		eq1.applyLocalRule("00", rule1);
		assertTrue(eq1.toString().equals("(((b*c)+(a*c))*w)#((b*d)+(b*d))"));

		eq1.applyLocalRule("00", rule1.getSwappedInstance());
		assertTrue(eq1.toString().equals("(((a*c)+(b*c))*w)#((b*d)+(b*d))"));

		eq1.applyLocalRule("1", rule2);
		assertTrue(eq1.toString().equals("(((a*c)+(b*c))*w)#(2*(b*d))"));

		eq1.applyLocalRule("1", rule2.getSwappedInstance());
		assertTrue(eq1.toString().equals("(((a*c)+(b*c))*w)#((b*d)+(b*d))"));
	}

	/**
	 * Test the isGlobalRuleValid function of the Equation Class.
	 * @see modele.metier.Equation#isGlobalRuleValid()
	 */
	@Test
	public void testIsGlobalRuleValid() {
		Variable a = new Variable("a");
		Variable b = new Variable("b");
		Variable c = new Variable("c");
		Variable d = new Variable("d");
		Variable w = new Variable("w");

		Operator plus = new Operator("plus", "+", "#", 2);
		Operator fois = new Operator("fois", "*", "#", 2);
		Operator moins = new Operator("moins", "-", "#", 2);
		Operator camion = new Operator("camion", "$", "#", 1);

		UnaryExpression expUn = new UnaryExpression(b, camion);

		BinaryExpression expLeftFois1 = new BinaryExpression(a, c, fois);
		BinaryExpression expRightFois1 = new BinaryExpression(expUn, d, fois);
		BinaryExpression expRightFois2 = new BinaryExpression(expUn, c, fois);

		BinaryExpression expEgaleLeft1 = new BinaryExpression(a, b, plus);
		BinaryExpression expEgaleRight1 = new BinaryExpression(c, a, moins);
		
		BinaryExpression expLeftPlus = new BinaryExpression(expLeftFois1, expRightFois2, plus);
		BinaryExpression expLeftFois = new BinaryExpression(expLeftPlus, w, fois);
		BinaryExpression expRightPlus = new BinaryExpression(expRightFois1, expRightFois1, plus);

		Equation eq1 = new Equation(expLeftFois, expRightPlus);

		Equation ruleGlobaleLeft = new Equation(c, expEgaleLeft1);
		Equation ruleGlobaleRight = new Equation(expEgaleRight1, b);

		assertTrue(eq1.isGlobalRuleValid(ruleGlobaleLeft, ruleGlobaleRight));
		eq1.applyGlobalRule(ruleGlobaleLeft, ruleGlobaleRight);

		assertTrue(eq1.isGlobalRuleValid(ruleGlobaleRight, ruleGlobaleLeft));
		eq1.applyGlobalRule(ruleGlobaleRight, ruleGlobaleLeft);
	}

	/**
	 * Test the applyGlobalRule function of the Equation Class.
	 * @see modele.metier.Equation#applyGlobalRule()
	 */
	@Test
	public void testApplyGlobalRule() {
		Variable a = new Variable("a");
		Variable b = new Variable("b");
		Variable c = new Variable("c");
		Variable d = new Variable("d");
		Variable w = new Variable("w");

		Operator plus = new Operator("plus", "+", "#", 2);
		Operator fois = new Operator("fois", "*", "#", 2);
		Operator moins = new Operator("moins", "-", "#", 2);
		Operator camion = new Operator("camion", "$", "#", 1);

		UnaryExpression expUn = new UnaryExpression(b, camion);

		BinaryExpression expLeftFois1 = new BinaryExpression(a, c, fois);
		BinaryExpression expRightFois1 = new BinaryExpression(expUn, d, fois);
		BinaryExpression expRightFois2 = new BinaryExpression(expUn, c, fois);

		BinaryExpression expEgaleLeft1 = new BinaryExpression(a, b, plus);
		BinaryExpression expEgaleRight1 = new BinaryExpression(c, a, moins);
		
		BinaryExpression expLeftPlus = new BinaryExpression(expLeftFois1, expRightFois2, plus);
		BinaryExpression expLeftFois = new BinaryExpression(expLeftPlus, w, fois);
		BinaryExpression expRightPlus = new BinaryExpression(expRightFois1, expRightFois1, plus);

		Equation eq1 = new Equation(expLeftFois, expRightPlus);

		Equation ruleGlobaleLeft = new Equation(c, expEgaleLeft1);
		Equation ruleGlobaleRight = new Equation(expEgaleRight1, b);

		eq1.applyGlobalRule(ruleGlobaleLeft, ruleGlobaleRight);
		assertTrue(eq1.toString().equals("((((a*c)+(($b)*c))*w)-(($b)*d))#(($b)*d)"));
		
		eq1.applyGlobalRule(ruleGlobaleRight, ruleGlobaleLeft);
		assertTrue(eq1.toString().equals("(((a*c)+(($b)*c))*w)#((($b)*d)+(($b)*d))"));
	}

	/**
	 * Test the containsExpression function of the Equation Class.
	 * @see modele.metier.Equation#containsExpression()
	 */
	@Test
	public void	testContainsExpression() {
		Variable a = new Variable("a");
		Variable b = new Variable("b");
		Variable c = new Variable("c");
		Variable d = new Variable("d");
		Variable w = new Variable("w");

		LiteralConstant deux = new LiteralConstant(2); 

		Operator plus = new Operator("plus", "+", "#", 2);
		Operator fois = new Operator("fois", "*", "#", 2);

		BinaryExpression expLeftFois1 = new BinaryExpression(a, c, fois);
		BinaryExpression expLeftFois2 = new BinaryExpression(a, a, plus);
		BinaryExpression expLeftFois3 = new BinaryExpression(deux, a, fois);
		BinaryExpression expRightFois1 = new BinaryExpression(b, d, fois);
		BinaryExpression expRightFois2 = new BinaryExpression(b, c, fois);
		BinaryExpression expLeftPlus = new BinaryExpression(expLeftFois1, expRightFois2, plus);
		BinaryExpression expLeftFois = new BinaryExpression(expLeftPlus, w, fois);
		BinaryExpression expRightPlus = new BinaryExpression(expRightFois1, expRightFois1, plus);
		BinaryExpression expLeftPlus1 = new BinaryExpression(a, b, plus);
		BinaryExpression expLeftPlus2 = new BinaryExpression(b, a, plus);

		Equation eq1 = new Equation(expLeftFois, expRightPlus);
		Equation rule1 = new Equation(expLeftPlus1, expLeftPlus2);
		Equation rule2 = new Equation(expLeftFois2, expLeftFois3);
		
		eq1.applyLocalRule("00", rule1);
		assertFalse(eq1.containsExpression(expLeftPlus));

		eq1.applyLocalRule("00", rule1.getSwappedInstance());
		assertTrue(eq1.containsExpression(expLeftPlus));

		eq1.applyLocalRule("1", rule2);
		assertTrue(eq1.containsExpression(deux));

		eq1.applyLocalRule("1", rule2.getSwappedInstance());
		assertFalse(eq1.containsExpression(deux));
	}
	
	//TODO Test of function getSwappedInstance
}
