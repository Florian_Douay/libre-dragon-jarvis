package modele.metier.test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import org.junit.Test;
import modele.metier.LiteralConstant;
import modele.metier.Variable;

/**
 * Test the functions of the Variable Class.
 * 
 * @since 1.0
 * @see modele.metier.Variable
 * 
 */

public class VariableTest {

	/**
	 * Test the equals function of the Variable Class.
	 * @see modele.metier.Variable#equals()
	 */
	@Test
	public void testEquals() {
		
		Variable varB1 = new Variable("b");
		Variable varB2 = new Variable("b");
		Variable varC = new Variable("c");
		LiteralConstant cstTwo1 = new LiteralConstant(2);
		LiteralConstant cstTwo2 = new LiteralConstant(2); 
		LiteralConstant cstThree = new LiteralConstant(3);

		assertTrue(varB1.equals(varB2));
		assertFalse(varB1.equals(varC));
		assertTrue(cstTwo1.equals(cstTwo2));
		assertFalse(cstTwo1.equals(cstThree));
	}
}
