package modele.metier.test;

import static org.junit.Assert.*;

import org.junit.Test;

import modele.metier.BinaryExpression;
import modele.metier.LiteralConstant;
import modele.metier.Operator;
import modele.metier.UnaryExpression;
import modele.metier.Variable;

/**
 * Test the functions of the BinaryExpression Class.
 * 
 * @since 1.0
 * @see modele.metier.BinaryExpression
 * 
 */

public class BinaryExpressionTest {

	/**
	 * Test the equals function of the BinaryExpression Class.
	 * @see modele.metier.BinaryExpression#equals()
	 */
	@Test
	public void testEquals() {
		Variable varB1 = new Variable("b");
		Variable varB2 = new Variable("b");
		Variable varC = new Variable("c");

		LiteralConstant cstTwo1 = new LiteralConstant(2);
		LiteralConstant cstTwo2 = new LiteralConstant(2); 
		LiteralConstant cstThree = new LiteralConstant(3);

		Operator plus = new Operator("plus", "+", "#", 2);
		Operator fois = new Operator("fois", "*", "#", 2);
		Operator moins = new Operator("moins", "-", "#", 2);
		Operator camion = new Operator("camion", "$", "#", 1);

		UnaryExpression expUn = new UnaryExpression(cstThree, camion);

		BinaryExpression expFois1 = new BinaryExpression(varB1, varC, fois);
		BinaryExpression expFois2 = new BinaryExpression(varB2, varC, fois);
		BinaryExpression expPlus1 = new BinaryExpression(expFois1, cstTwo1, plus);
		BinaryExpression expPlus2 = new BinaryExpression(expFois2, cstTwo2, plus);
		BinaryExpression expFinal1 = new BinaryExpression(expUn, expPlus1, moins);
		BinaryExpression expFinal2 = new BinaryExpression(expUn, expPlus2, moins);
		
		assertTrue(expFois1.equals(expFois2));
		assertTrue(expPlus1.equals(expPlus2));
		assertTrue(expFinal1.equals(expFinal2));
		assertFalse(expFois1.equals(expPlus1));
		assertFalse(expPlus1.equals(expFinal1));
	}
}
