package modele.metier;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Observable;

/**
 * Model class that defines a GameLevel.
 * <p>
 * It contains : 
 * <ul>
 * <li>An unique id</li>
 * <li>The equation of the game level to resolve</li>
 * <li>The list of allowed Operators by the level</li>
 * <li>The list of allowed Variables and LiteralConstants by the level</li>
 * </ul>
 * @since 1.0
 * @see Equation
 * @see Operator
 * @see Variable
 * @see LiteralConstant
 * 
 */

public class GameLevel extends Observable {
	
	/**
	 * The static counter in order to provide unique id for each instance.
	 */
	private static int levelIdCounter = 0;

	/**
	 * The unique id for this instance.
	 */
	final private int levelId;
	
	/**
	 * The initial equation to resolve.
	 */
	final private Equation equation;

	/**
	 * The container of all of allowed Operators by the level.
	 */
	final private HashMap<String,Operator> levelOperatorMap;
	
	/**
	 * The container of all of allowed Variables and LiteralConstants by the level.
	 */
	final private ArrayList<TerminalExpression> levelPileArray;
	
	/**
	 * Constructor of the GameLevel.
	 * @param equation the equation of the game level to resolve.
	 * @param levelOperatorMap the container operators allowed by the level.
	 * @param levelPileArray the container pile elements allowed by the level.
	 */
	public GameLevel (Equation equation, 
					  HashMap<String,Operator> levelOperatorMap,
					  ArrayList<TerminalExpression> levelPileArray)
	{
		this.equation = equation;
		this.levelId = ++levelIdCounter;
		this.levelOperatorMap = levelOperatorMap;
		this.levelPileArray = levelPileArray;
	}
	
	/**
	 * Copy Constructor of the GameLevel.  It's a deep copy.
	 * @param gameLevel the gameLevel to copy.
	 */
	public GameLevel (GameLevel gameLevel) {
		this.equation = new Equation(gameLevel.getEquation());
		this.levelId = new Integer(gameLevel.getLevelId());
		this.levelOperatorMap = new HashMap<String,Operator>(gameLevel.levelOperatorMap);
		this.levelPileArray = new ArrayList<TerminalExpression>(gameLevel.levelPileArray);
	}
	
	/**
	 * Getter of the GameLevel unique id.
	 * @return the GameLevel unique id.
	 */
	public final int getLevelId() {
		return levelId;
	}
	
	/**
	 * Getter of the GameLevel's Expression.
	 * @return the GameLevel's Expression.
	 */
	public final Equation getEquation() {
		return equation;
	}
	
	/**
	 * Getter of the GameLevel's allowed operators.
	 * @return the GameLevel's allowed operators.
	 */
	public final HashMap<String,Operator> getLevelOperators() {
		return levelOperatorMap;
	}
	
	/**
	 * Getter of the GameLevel's allowed pile elements.
	 * @return the GameLevel's allowed pile elements.
	 */
	public final ArrayList<TerminalExpression> getLevelPile() {
		return levelPileArray;
	}
	
	/**
	 * Returns if the current game level is won.
	 * @return true if the current game level is won. false if not won or no game level loaded.
	 */
	public Boolean isLevelWon() {
		String expLeft = this.getEquation().getLeftExpr().toString();
		String expRight = this.getEquation().getRightExpr().toString();
		
		if ((expLeft.equals("x") && expRight.indexOf("x") == -1) 
			||
			(expRight.equals("x") && expLeft.indexOf("x") == -1)) {
				return true;
			}
			
		return false;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		
		String str = new String();

		str += "[";
		str += Integer.toString(levelId);
		str += ", ";
		str += "[" + this.getEquation().toString() + "]" + this.getEquation().getTreePathIDs().toString();
		str += ", ";
		str += levelOperatorMap.values().toString();
		str += ", ";
		str += levelPileArray.toString();
		str += "]";
		
		return str;
	}
	
	/**
	 * Returns the GameLevel as array of strings.
	 * @return the GameLevel as array of strings.
	 */
	public String[] toStrings() {
		String[] gameLevelsStrings = new String [5];
		
		ArrayList<String> pathIdsArray = this.getEquation().getTreePathIDs();
		
		gameLevelsStrings[0] = Integer.toString(levelId);
		gameLevelsStrings[1] = "[" + this.getEquation().toString() + "]";
		gameLevelsStrings[2] = pathIdsArray.toString();
		gameLevelsStrings[3] = levelOperatorMap.values().toString();
		gameLevelsStrings[4] = levelPileArray.toString();
		
		return gameLevelsStrings;
	}
	
	/**
	 * Returns the GameLevel as array of strings with comma separators in equation.
	 * @return the GameLevel as array of strings with comma separators in equation.
	 */
	public String[] toStringsWithSeparators() {
		String[] gameLevelsStrings = new String [5];
		
		ArrayList<String> pathIdsArray = this.getEquation().getTreePathIDs();
		
		gameLevelsStrings[0] = Integer.toString(levelId);
		gameLevelsStrings[1] = "[" + this.getEquation().toStringWithSeparators() + "]";
		gameLevelsStrings[2] = pathIdsArray.toString();
		gameLevelsStrings[3] = levelOperatorMap.values().toString();
		gameLevelsStrings[4] = levelPileArray.toString();
		
		return gameLevelsStrings;
	}
	
	/**
	 * Returns if the given model path is existing in the game level.
	 * @param modelPath the model path to check the existence.
	 * @return true if the given model path is existing in the game level. false otherwise.
	 */
	public boolean isPathExistingInModel(String modelPath) {
		
		Expression exp = this.getEquation().search(modelPath);
		
		return (exp != null);
	}
}
