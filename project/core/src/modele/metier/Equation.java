package modele.metier;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

/**
 * Container used to stock two Expressions with an idea of equality between the two.
 * <p>
 * It allows :
 * <ul>
 * <li>To get or set one of the Expressions,</li>
 * <li>To access/change/search one part (node) with a path,</li>
 * <li>To test if it's possible to apply a rule on a node, and if it's the case, apply it,</li>
 * <li>To get the Equation in form of a string and have the ID for each node.</li>
 * </ul>
 * 
 * @since 1.0
 * @see modele.metier.Expression
 * @see modele.metier.LocalRule
 * @see modele.metier.GlobalRule
 * 
 */

public class Equation {

	/**
	 * Left Expression of the Equation.
	 */
	private Expression leftExpr;
	/**
	 * Right Expression of the Equation.
	 */
	private Expression rightExpr;

	/**
	 * Create an instance of Equation by using two Expressions (copy).
	 * @param leftExpr Left Expression of the Equation.
	 * @param rightExpr Right Expression of the Equation.
	 */
	public Equation (Expression leftExpr, Expression rightExpr) {
		this.leftExpr = leftExpr.clone();
		this.rightExpr = rightExpr.clone();
	}

	/**
	 * Copy constructor of Equation.
	 * @param equation Equation copied to create the new Equation.
	 */
	public Equation (Equation equation) {
		this.leftExpr = (Expression) equation.leftExpr.clone();
		this.rightExpr = (Expression) equation.rightExpr.clone();
	}

	/**
	 * Getter of the left Expression.
	 * @return Left Expression of the Equation.
	 */
	public final Expression getLeftExpr() {
		return leftExpr;
	}

	/**
	 * Getter of the right Expression.
	 * @return Right Expression of the Equation.
	 */
	public final Expression getRightExpr() {
		return rightExpr;
	}

	/**
	 * Setter of the left Expression. Replace the left Expression with the parameter.
	 * @param exp Expression used to replace the one stored.
	 */
	private void setLeftExpr(Expression exp) {
		this.leftExpr = exp.clone();
	}

	/**
	 * Setter of the left Expression. Replace the left Expression with the parameter.
	 * @param exp Expression used to replace the one stored.
	 */
	private void setRightExpr(Expression exp) {
		this.rightExpr = exp.clone();
	}

	/**
	 * Return an Expression using the path given.
	 * @param modelPath Path of the wanted Expression.
	 * @return The Expression at the given path. Null if the path given is wrong and reach a non-existing node.
	 */
	public Expression search(String modelPath) {
		if (modelPath.substring(0, 1).equals("0")) {
			if (modelPath.length() == 1)
				return leftExpr;
			return search(modelPath.substring(1, modelPath.length()), leftExpr);
		}

		if (modelPath.length() == 1)
			return rightExpr;
		return search(modelPath.substring(1, modelPath.length()), rightExpr);
	}

	/**
	 * Recursive private-sub-function used by the main search function.
	 * @param modelPath Path of the wanted Expression.
	 * @param currentExpr Current node where the function is working.
	 * @return The Expression at the given path. Null if the path given is wrong and reach a non-existing node.
	 */
	private Expression search(String modelPath, Expression currentExpr) {
		if (currentExpr.getType() == ExpressionType.BINARY_EXPRESSION) { 
			BinaryExpression expB = (BinaryExpression) currentExpr;

			if (modelPath.substring(0, 1).equals("0")) {
				if (modelPath.length() == 1)
					return expB.getLeftExpr();
				return search(modelPath.substring(1, modelPath.length()), expB.getLeftExpr());
			}

			if (modelPath.length() == 1)
				return expB.getRightExpr();
			return search(modelPath.substring(1, modelPath.length()), expB.getRightExpr());
		}
		else if (currentExpr.getType() == ExpressionType.UNARY_EXPRESSION) {
			UnaryExpression expU = (UnaryExpression) currentExpr;

			if (modelPath.substring(0, 1).equals("0")) {
				if (modelPath.length() == 1)
					return currentExpr;
				return search(modelPath.substring(1, modelPath.length()), expU.getExpression());
			}

			return null;
		}

		return null;
	}

	/**
	 * Replace an Expression at the path given with the new Expression in parameter.
	 * @param modelPath Path of the Expression to replace.
	 * @param exprToPlace The Expression used to replace the old one.
	 * @return False if the path given is wrong and reach a non-existing node. True otherwise.
	 */
	public boolean replace (String modelPath, Expression exprToPlace) {
		if (modelPath.substring(0, 1).equals("0")) {
			if (modelPath.length() == 1) {
				setLeftExpr(exprToPlace);
				return true;
			}
			else
				return replace(modelPath.substring(1, modelPath.length()), exprToPlace, leftExpr);
		}
		else {
			if (modelPath.length() == 1) {
				setRightExpr(exprToPlace);
				return true;
			}
			else
				return replace(modelPath.substring(1, modelPath.length()), exprToPlace, rightExpr);
		}
	}

	/**
	 * Recursive private-sub-function used by the main replace function.
	 * @param modelPath Path of the Expression to replace.
	 * @param exprToPlace The Expression used to replace the old one.
	 * @param currentExpr Current node where the function is working.
	 * @return False if the path given is wrong and reach a non-existing node. True otherwise.
	 */
	private boolean replace (String modelPath, Expression exprToPlace, Expression currentExpr) {
		if (currentExpr.getType() == ExpressionType.BINARY_EXPRESSION) { 
			BinaryExpression expB = (BinaryExpression) currentExpr;

			if (modelPath.substring(0, 1).equals("0")) {
				if (modelPath.length() == 1) {
					expB.setLeftExpr(exprToPlace);
					return true;
				}
				else
					return replace(modelPath.substring(1, modelPath.length()), exprToPlace, expB.getLeftExpr());
			}
			else {
				if (modelPath.length() == 1) {
					expB.setRightExpr(exprToPlace);
					return true;
				}
				else
					return replace(modelPath.substring(1, modelPath.length()), exprToPlace, expB.getRightExpr());
			}
		}
		else if (currentExpr.getType() == ExpressionType.UNARY_EXPRESSION) {
			UnaryExpression expU = (UnaryExpression) currentExpr;

			if (modelPath.substring(0, 1).equals("0")) {
				if (modelPath.length() == 1) {
					expU.setExpression(exprToPlace);
					return true;
				}
				else
					return replace(modelPath.substring(1, modelPath.length()), exprToPlace, expU.getExpression());
			}
			else
				return false;
		}
		else
			return false;
	}

	/**
	 * Give for each object in the Equation its ID.
	 * @return Array of Strings storing every IDs in order of apparition when you read the Equation.
	 */
	public ArrayList<String> getTreePathIDs() {
		ArrayList<String> tree = new ArrayList<String>();

		getTreePathIDs(leftExpr, "0", tree);
		tree.add("2");
		getTreePathIDs(rightExpr, "1", tree);

		return tree;
	}

	/**
	 * Recursive private-sub-function used by the main getTreePathIDs function. 
	 * @param currentExpr Current node where the function is working.
	 * @param modelPath Path of the current node.
	 * @param tree Array of String storing the IDs. Object returned in the main function.
	 */
	private void getTreePathIDs(Expression currentExpr, String modelPath, ArrayList<String> tree) {
		if (currentExpr.getType() == ExpressionType.BINARY_EXPRESSION) {
			BinaryExpression expB = (BinaryExpression) currentExpr;
			tree.add(modelPath);
			getTreePathIDs(expB.getLeftExpr(), modelPath.concat("0"), tree);
			tree.add(modelPath);
			getTreePathIDs(expB.getRightExpr(), modelPath.concat("1"), tree);
			tree.add(modelPath);
		}
		else if (currentExpr.getType() == ExpressionType.UNARY_EXPRESSION) {
			UnaryExpression expU = (UnaryExpression) currentExpr;
			tree.add(modelPath);
			tree.add(modelPath);
			getTreePathIDs(expU.getExpression(), modelPath.concat("0"), tree);
			tree.add(modelPath);
		}
		else
			tree.add(modelPath);
	}

	/**
	 * Give for each variable all of its IDs in the Equation.
	 * @return HashMap associating an Expression with an array of Strings (IDs).
	 */
	public HashMap<Expression, ArrayList<String>> getVarPathIDs() {
		HashMap<Expression, ArrayList<String>> treeMap = new HashMap<Expression, ArrayList<String>>();
		getVarPathIDs(leftExpr, "0", treeMap);
		getVarPathIDs(rightExpr, "1", treeMap);

		return treeMap;
	}

	/**
	 * Recursive private-sub-function used by the main getVarPathIDs function. 
	 * @param currentExpr Current node where the function is working.
	 * @param modelPath Path of the current node.
	 * @param treeMap HashMap storing every IDs for every Variable in the Equation. Object returned in the main function.
	 */
	private void getVarPathIDs(Expression currentExpr, String modelPath, HashMap<Expression, ArrayList<String>> treeMap) {
		if (currentExpr.getType() == ExpressionType.BINARY_EXPRESSION) {
			BinaryExpression expB = (BinaryExpression) currentExpr; 
			getVarPathIDs(expB.getLeftExpr(), modelPath.concat("0"), treeMap);
			getVarPathIDs(expB.getRightExpr(), modelPath.concat("1"), treeMap);
		}
		else if (currentExpr.getType() == ExpressionType.UNARY_EXPRESSION) {
			UnaryExpression expU = (UnaryExpression) currentExpr;
			getVarPathIDs(expU.getExpression(), modelPath.concat("0"), treeMap);
		}
		else if (currentExpr.getType() == ExpressionType.VARIABLE) {
			ArrayList<String> tmpArray = treeMap.get(currentExpr);
			if (tmpArray == null)
				tmpArray = new ArrayList<String>();
			tmpArray.add(modelPath);
			treeMap.put(currentExpr, tmpArray);
		}
	}
	
	/**
	 * Determine if a rule can be used on a node. Said also if we must use the right or left part of the rule.
	 * @param exprToMatch Node where we apply the rule.
	 * @param exprRule Rule to test on the node.
	 * @return True if the rule can be used. False otherwise.
	 */
	public boolean isLocalRuleValid (Expression exprToMatch, Expression exprRule) {
		if ( exprToMatch == null )
			return false;

		HashMap<Expression, Expression> leafMap = new HashMap<Expression, Expression>();
		return isLocalRuleValid(exprToMatch, exprRule, leafMap);
	}

	/**
	 * Determine if a rule can be used on a node. Said also if we must use the right or left part of the rule.
	 * @param modelPath Path used to find the node where the rule will be tested.
	 * @param rule Rule to test on the node.
	 * @return True if the rule can be used. False otherwise.
	 */
	public boolean isLocalRuleValid (String modelPath, Equation rule) {
		Expression nodeExpr = search(modelPath);
		if ( nodeExpr == null )
			return false;

		HashMap<Expression, Expression> leafMap = new HashMap<Expression, Expression>();
		return isLocalRuleValid(nodeExpr, rule.getLeftExpr(), leafMap);
	}

	/**
	 * Recursive private-sub-function used by the main isLocalRuleValid function. Test on sub-part of the rule.
	 * @param exprToMatch Current Expression where the function is working.
	 * @param ruleExpr Sub-Part of the original rule to test.
	 * @param leafMap For each Variable in the rule associates the Expression at the same place in the Expression. Used by the applyRule function.
	 * @return True if the rule can be used. False otherwise.
	 */
	private boolean isLocalRuleValid (Expression exprToMatch, Expression ruleExpr, HashMap<Expression, Expression> leafMap) {
		if (ruleExpr.getType() == ExpressionType.BINARY_EXPRESSION &&
			exprToMatch.getType() == ExpressionType.BINARY_EXPRESSION) {

			BinaryExpression ruleBinaryExpr = (BinaryExpression) ruleExpr;
			BinaryExpression nodeBinaryExpr = (BinaryExpression) exprToMatch;
			if ( ! ruleBinaryExpr.getOperator().equals(nodeBinaryExpr.getOperator()))
				return false;

			boolean testLeftRule = isLocalRuleValid(nodeBinaryExpr.getLeftExpr(), ruleBinaryExpr.getLeftExpr(), leafMap);
			boolean testRightRule = isLocalRuleValid(nodeBinaryExpr.getRightExpr(), ruleBinaryExpr.getRightExpr(), leafMap);
			if ( testLeftRule && testRightRule )
				return true;
			return false;
		}
		else if (ruleExpr.getType() == ExpressionType.UNARY_EXPRESSION &&
				 exprToMatch.getType() == ExpressionType.UNARY_EXPRESSION) {

			UnaryExpression ruleUnaryExpr = (UnaryExpression) ruleExpr;
			UnaryExpression nodeUnaryExpr = (UnaryExpression) exprToMatch;
			if ( ! ruleUnaryExpr.getOperator().equals(nodeUnaryExpr.getOperator()))
				return false;

			return isLocalRuleValid(nodeUnaryExpr.getExpression(), ruleUnaryExpr.getExpression(), leafMap);
		}
		else if (ruleExpr.getType() == ExpressionType.VARIABLE) {
			Expression nodeExprString = leafMap.get(ruleExpr.toString()); 
			if (nodeExprString == null) {
				leafMap.put(ruleExpr, exprToMatch);
				return true;
			}
			else {
				if ( ! exprToMatch.equals(nodeExprString))
					return false;
				return true;
			}
		}
		else if (ruleExpr.getType() == ExpressionType.LITERAL_CONSTANT &&
				 exprToMatch.getType() == ExpressionType.LITERAL_CONSTANT) {

			LiteralConstant ruleUnaryExpr = (LiteralConstant) ruleExpr;
			LiteralConstant nodeUnaryExpr = (LiteralConstant) exprToMatch;
			if (ruleUnaryExpr.getValue() != nodeUnaryExpr.getValue())
				return false;
			return true;
		}
		return false;
	}

	/**
	 * Determine if a rule can be used on a node. Apply if it can be used.
	 * @param modelPath Path to the Expression to test.
	 * @param ruleEquation Equation of the rule to apply.
	 * @return True if the rule was used. False otherwise, same result if the node does not exist.
	 */
	public boolean applyLocalRule (String modelPath, Equation ruleEquation) {
		Equation ruleTmp = new Equation(ruleEquation);
		Expression ruleExpr = ruleTmp.getLeftExpr();

		Expression nodeExpr = search(modelPath);
		if ( nodeExpr == null )
			return false;

		HashMap<Expression, Expression> leafMap = new HashMap<Expression, Expression>();
		if (isLocalRuleValid(nodeExpr, ruleExpr, leafMap) == false)
			return false;

		HashMap<Expression, ArrayList<String>> hashTree = ruleTmp.getVarPathIDs();
		for(Entry<Expression, ArrayList<String>> entry : hashTree.entrySet()) {
			Expression key = entry.getKey();
		    ArrayList<String> values = entry.getValue();

		    for(int i=0; i < values.size(); i++)
		    	ruleTmp.replace(values.get(i), leafMap.get(key));
		}

		Expression replaceExpr = ruleTmp.getRightExpr();

		replace(modelPath, replaceExpr);

		return true;
	}

	/**
	 * Determine if a global rule can be used on a node. Said also if we must use the right or left part of the global rule.
	 * @param eqToMatch Left side of the global rule to test.
	 * @param eqToTransform Right side of the global rule to test.
	 * @return True if the rule can be used. False otherwise.
	 */
	public boolean isGlobalRuleValid (Equation eqToMatch, Equation eqToTransform) {
		HashMap<Expression, Expression> leafMap = new HashMap<Expression, Expression>();
		if ( isGlobalRuleValid(eqToMatch.getLeftExpr(), eqToMatch.getRightExpr(), leafMap) )
			return true;

		return false;
	}

	/**
	 * Recursive private-sub-function used by the main isGlobalRuleValid function. Test on sub-part of the global rule.
	 * @param ruleExprLeft Left expression of the rule to test.
	 * @param ruleExprRight Right expression of the rule to test.
	 * @param leafMap For each Variable in the rule associates the Expression at the same place in the Expression. Used by the applyRule function.
	 * @return True if the rule can be used. False otherwise.
	 */
	private boolean isGlobalRuleValid (Expression ruleExprLeft, Expression ruleExprRight, HashMap<Expression, Expression> leafMap) {
		if (isLocalRuleValid(leftExpr,  ruleExprLeft,  leafMap) == false ||
			isLocalRuleValid(rightExpr, ruleExprRight, leafMap) == false)
				return false;
		return true;
	}

	/**
	 * Determine if a global rule can be used on a node. Apply if it can be used.
	 * @param ruleToMatch Left side of the global rule to apply.
	 * @param ruleToTransform Right side of the global rule to apply.
	 * @return True if the rule was used. False otherwise.
	 */
	public boolean applyGlobalRule (Equation ruleToMatch, Equation ruleToTransform) {
		Equation eqToMatch = new Equation(ruleToMatch);
		Equation eqToTransform = new Equation(ruleToTransform);

		HashMap<Expression, Expression> leafMap = new HashMap<Expression, Expression>();
		if (isGlobalRuleValid(eqToMatch.getLeftExpr(), eqToMatch.getRightExpr(), leafMap) == false)
			return false;

		HashMap<Expression, ArrayList<String>> hashTree = eqToTransform.getVarPathIDs();
		for(Entry<Expression, ArrayList<String>> entry : hashTree.entrySet()) {
			Expression key = entry.getKey();
		    ArrayList<String> values = entry.getValue();

		    for(int i=0; i < values.size(); i++)
	    		if (eqToMatch.containsExpression(key))
	    			eqToTransform.replace(values.get(i), leafMap.get(key));
		}

		replace("0", eqToTransform.getLeftExpr());
		replace("1", eqToTransform.getRightExpr());

		return true;
	}

	/**
	 * Test if the parameter exists in the Equation.
	 * @param expToFind the Expression to find.
	 * @return True if the parameter exists in the Equation. False otherwise.
	 */
	public boolean containsExpression(Expression expToFind) {
		if (expToFind.equals(leftExpr))
			return true;
		
		if (containsExpression(leftExpr, expToFind))
			return true;
		
		if (expToFind.equals(rightExpr))
			return true; 
		
		return containsExpression(rightExpr, expToFind);
	}

	/**
	 * Recursive private-sub-function used by the main containsExpression function. Test on sub-part of the Equation.
	 * @param currentExpr the actual node in the Equation.
	 * @param expToFind the Expression to find.
	 * @return True if the parameter exists in the Equation. False otherwise.
	 */
	private boolean containsExpression(Expression currentExpr, Expression expToFind) {
		if (currentExpr.getType() == ExpressionType.BINARY_EXPRESSION) {
			BinaryExpression expB = (BinaryExpression) currentExpr;
			if (containsExpression(expB.getLeftExpr(), expToFind))
				return true;
			if (containsExpression(expB.getRightExpr(), expToFind))
				return true;
		}
		else if (currentExpr.getType() == ExpressionType.UNARY_EXPRESSION) {
			UnaryExpression expU = (UnaryExpression) currentExpr;
			if (containsExpression(expU.getExpression(), expToFind))
				return true;
		}
		return currentExpr.equals(expToFind);
	}
 	
 	/**
	 * Return the Equation with the left and right expressions swapped.
	 * @return New Equation with the left and right expressions swapped.
	 */
 	public Equation getSwappedInstance() {
 		return new Equation(this.getRightExpr(), this.getLeftExpr());
 	}

	/**
	 * Return the String corresponding to the Equation with separators between each elements.
	 * @return String corresponding to the Equation with separators.
	 */
	public String toStringWithSeparators() {
		return leftExpr.toStringWithSeparator() + "," + "#" + "," + rightExpr.toStringWithSeparator();
	}

	
 	/* (non-Javadoc)
 	 * @see java.lang.Object#toString()
 	 */
 	@Override
	public String toString() {
		return leftExpr.toString() + "#" + rightExpr.toString();
	}
}
