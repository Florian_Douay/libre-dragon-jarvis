package modele.metier;

/**
 * Expression is a mathematics expression with variables, operators and constants.
 * General class extended to define usable expressions.
 * Implements Cloneable in order to use the design pattern clone.
 * 
 * @since 1.0
 * 
 */
public abstract class Expression implements Cloneable {

	/**
	 * Type of the Expression, used with polymorphism to determine the type of the current Expression.
	 */
	private ExpressionType type;

	/**
	 * Create an instance of Expression by using the ExpressionType.
	 * @param type ExpressionType of the Expression.
	 */
	public Expression(ExpressionType type) {
		this.type = type;
	}

	/**
	 * Getter of the ExpressionType.
	 * @return ExpressionType of the Expression.
	 */
	public final ExpressionType getType() {
		return type;
	}
	
	/**
	 * Return the String corresponding to the Expression with separators between each elements.
	 * @return String corresponding to the Expression with separators.
	 */
	public abstract String toStringWithSeparator();

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public abstract String toString();

	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	public abstract Expression clone();

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public abstract int hashCode();

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public abstract boolean equals(Object obj);
}
