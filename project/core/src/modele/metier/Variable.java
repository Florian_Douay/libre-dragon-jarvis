package modele.metier;

/**
 * TerminalExpression used to store variables.
 * 
 * @since 1.0
 * @see modele.metier.Expression
 * @see modele.metier.TerminalExpression
 * 
 */

public class Variable extends TerminalExpression {
	
	/**
	 * Value of the Variable.
	 */
	private String var;
	
	/**
	 * Create an instance of Variable by using string (value).
	 * @param var Value of the Variable.
	 */
	public Variable (String var) {
		super(ExpressionType.VARIABLE);
		this.var = var;
	}
	
	/**
	 * Copy constructor of Variable.
	 * @param variable Variable copied to create the new Variable.
	 */
	public Variable(Variable variable) {
		super(ExpressionType.VARIABLE);
		this.var = new String(variable.var);
	}

	/**
	 * Getter of the value.
	 * @return Value of the Variable.
	 */
	public String getVar() {
		return var;
	}
	
	/* (non-Javadoc)
	 * @see modele.metier.Expression#toStringWithSeparator()
	 */
	@Override
	public String toStringWithSeparator() {
		return toString();
	}

	/* (non-Javadoc)
	 * @see modele.metier.TerminalExpression#toString()
	 */
	@Override
	public String toString() {
		return var;
	}
	
	/* (non-Javadoc)
	 * @see modele.metier.TerminalExpression#clone()
	 */
	@Override
	public Variable clone() {
		return new Variable(this);
	}

	/* (non-Javadoc)
	 * @see modele.metier.TerminalExpression#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((var == null) ? 0 : var.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see modele.metier.TerminalExpression#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Variable other = (Variable) obj;
		if (var == null) {
			if (other.var != null)
				return false;
		} else if (!var.equals(other.var))
			return false;
		return true;
	}
}
