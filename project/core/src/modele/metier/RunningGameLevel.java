package modele.metier;

/**
 * Model class that represents a loaded GameLevel.
 * <p>
 * It contains : 
 * <ul>
 * <li>An unique constructor which proceeds a deep copy of a GameLevel</li>
 * <li>A method to notify to the observer/observable pattern that model has changed</li>
 * </ul>
 * @since 1.0
 * @see GameLevel
 * 
 */

public class RunningGameLevel extends GameLevel {
	
	/**
	 * Constructor of the RunningGameLevel. Proceeds to a deep copy of a GameLevel to initialize itself.
	 * @param gameLevel the gameLevel to proceed the deep copy.
	 */
	public RunningGameLevel(GameLevel gameLevel) {
		super(new GameLevel(gameLevel));
		this.notifyObservers();
	}
	
	/**
	 * Sets the flag model changed for the pattern observer/observable.
	 */
	public void setModelChanged() {
		this.setChanged();
	}
}
