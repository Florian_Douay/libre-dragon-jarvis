package modele.metier;

/**
 * Model class that defines a GlobalRule with two Equations.
 * Used to transform an Equation in a new one.
 * 
 * @since 1.0
 * @see modele.metier.Rule
 * @see modele.metier.Equation
 * 
 */

public class GlobalRule extends Rule {
	
	/**
	 * Left Equation of the GlobalRule.
	 */
	private Equation leftEquation;
	/**
	 * Right Equation of the GlobalRule.
	 */
	private Equation rightEquation;
	
	/**
	 * Create an instance of GlobalRule by using two Equation.
	 * @param leftEquation Left Equation of the GlobalRule.
	 * @param rightEquation Right Equation of the GlobalRule.
	 */
	public GlobalRule (Equation leftEquation, Equation rightEquation) {
		this.leftEquation = leftEquation;
		this.rightEquation = rightEquation;
	}

	/**
	 * Getter of the left Equation of the GlobalRule.
	 * @return the left Equation of the GlobalRule.
	 */
	public final Equation getLeftEquation() {
		return leftEquation;
	}

	/**
	 * Getter of the right Equation of the GlobalRule.
	 * @return the right Equation of the GlobalRule.
	 */
	public final Equation getRightEquation() {
		return rightEquation;
	}
	
	/* (non-Javadoc)
	 * @see modele.metier.Rule#toString()
	 */
	@Override
	public String toString() {
		return leftEquation.toString() + " ## " + rightEquation.toString();
	}
}
