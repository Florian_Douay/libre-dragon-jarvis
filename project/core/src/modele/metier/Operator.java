package modele.metier;

/**
 * Used to store operators in Expressions and by extension Equations.
 * 
 * @since 1.0
 * @see modele.metier.Expression
 * @see modele.metier.Equation
 * 
 */

public class Operator {

	/**
	 * Name of the Operator.
	 */
	private String name;

	/**
	 * Symbol used in Expressions and Equations.
	 */
	private String symbol;

	/**
	 * Path to an image of the Operator.
	 */
	private String imgPath;

	/**
	 * Determine the number of Expression(s) used by the Operator.
	 * @see modele.metier.UnaryExpression
	 * @see modele.metier.BinaryExpression
	 */
	private int arity;

	/**
	 * Create an instance of Operator.
	 * @param name Name of the Operator.
	 * @param symbol Symbol used in Expressions and Equations.
	 * @param imgPath to an image of the Operator.
	 * @param arity the number of arguments needed by this operator.
	 */
	public Operator (String name, String symbol, String imgPath, int arity) {
		this.name = name;
		this.symbol = symbol;
		this.imgPath = imgPath;
		this.arity = arity;
	}

	/**
	 * Copy constructor of Operator.
	 * @param operator Operator copied to create the new Operator.
	 */
	public Operator (Operator operator) {
		this.name = new String(operator.name);
		this.symbol = new String(operator.symbol);
		this.imgPath = new String(operator.imgPath);
		this.arity = new Integer(operator.arity);
	}

	/**
	 * Getter of the Operator's name.
	 * @return Name of the Operator.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Getter of the Operator's symbol.
	 * @return Symbol of the Operator.
	 */
	public String getSymbol() {
		return symbol;
	}

	/**
	 * Getter of the path to the Operator's image.
	 * @return Path to the Operator's image.
	 */
	public String getImgPath() {
		return imgPath;
	}

	/**
	 * Getter of the Operator's arity.
	 * @return arity of the Operator.
	 */
	public int getArity() {
		return arity;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return symbol;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + arity;
		result = prime * result + ((imgPath == null) ? 0 : imgPath.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((symbol == null) ? 0 : symbol.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Operator other = (Operator) obj;
		if (arity != other.arity)
			return false;
		if (imgPath == null) {
			if (other.imgPath != null)
				return false;
		} else if (!imgPath.equals(other.imgPath))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (symbol == null) {
			if (other.symbol != null)
				return false;
		} else if (!symbol.equals(other.symbol))
			return false;
		return true;
	}
}
