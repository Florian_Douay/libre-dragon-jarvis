package modele.metier;

/**
 * Abstract class that permits to create rules.
 * Theses rules determines all the possibles manipulations on Expressions and Equations.  
 * 
 * @since 1.0
 * @see modele.metier.Expression
 * @see modele.metier.Equation
 * 
 */

public abstract class Rule {

	/**
	 * Counter for the ID of the new rules.
	 * static do not allows conflict on ID, ID in fact unique.
	 */
	private static int ruleIdCounter = 0;

	/**
	 * ID of the Rule (starting at 0).
	 */
	final private int ruleId;

	/**
	 * Create an instance of Rule.
	 * Determine the ID of the Rule.
	 */
	public Rule () {
		this.ruleId = ++ruleIdCounter;
	}

	/**
	 * Getter of the Rule's ID.
	 * @return ID of the Rule.
	 */
	public final int getRuleId() {
		return ruleId;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public abstract String toString();
}
