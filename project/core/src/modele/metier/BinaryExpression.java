package modele.metier;

/**
 * NonTerminalExpression that defines an Expression with one Operator and two Expressions.
 * 
 * @since 1.0
 * @see modele.metier.Expression
 * @see modele.metier.NonTerminalExpression
 * 
 */

public class BinaryExpression extends NonTerminalExpression {

	/**
	 * Left Expression of the BinaryExpression.
	 */
	private Expression leftExpr;
	/**
	 * Right Expression of the BinaryExpression.
	 */
	private Expression rightExpr;
	/**
	 * Operator of the BinaryExpression.
	 */
	private Operator operator;
	
	/**
	 * Constructor BinaryExpression.
	 * @param leftExpr the left Expression of the BinaryExpression.
	 * @param rightExpr the right Expression of the BinaryExpression.
	 * @param operator the operator of the BinaryExpression.
	 */
	public BinaryExpression (Expression leftExpr, Expression rightExpr, Operator operator) {
		super(ExpressionType.BINARY_EXPRESSION);
		this.leftExpr = leftExpr;
		this.rightExpr = rightExpr;
		this.operator = operator;
	}
	
	/**
	 * Copy constructor of BinaryExpression.
	 * @param binaryExpression BinaryExpression copied to create the new BinaryExpression.
	 */
	public BinaryExpression(BinaryExpression binaryExpression) {
		super(ExpressionType.BINARY_EXPRESSION);
		this.leftExpr = binaryExpression.leftExpr.clone();
		this.rightExpr = binaryExpression.rightExpr.clone();
		this.operator = new Operator(binaryExpression.operator);
	}
	
	/**
	 * Getter of the left Expression of the BinaryExpression.
	 * @return the left Expression of the BinaryExpression.
	 */
	public Expression getLeftExpr() {
		return leftExpr;
	}
	
	/**
	 * Getter of the right Expression of the BinaryExpression.
	 * @return the right Expression of the BinaryExpression.
	 */
	public Expression getRightExpr() {
		return rightExpr;
	}
	
	/**
	 * Getter of the operator of the BinaryExpression.
	 * @return the operator of the BinaryExpression.
	 */	
	public Operator getOperator() {
		return operator;
	}
	
	/**
	 * Setter of the left Expression.
	 * @param expLeft the Expression that replaces the left Expression.
	 */
	public void setLeftExpr(Expression expLeft) {
		this.leftExpr = expLeft;
	}
	
	/**
	 * Setter of the right Expression.
	 * @param expRight the Expression that replaces the right Expression.
	 */
	public void setRightExpr(Expression expRight) {
		this.rightExpr = expRight;
	}
	
	/* (non-Javadoc)
	 * @see modele.metier.Expression#toStringWithSeparator()
	 */
	@Override
	public String toStringWithSeparator() {
		return "(" + "," + leftExpr.toStringWithSeparator() + "," + operator.toString() + "," + rightExpr.toStringWithSeparator() + "," + ")";
	}

	/* (non-Javadoc)
	 * @see modele.metier.NonTerminalExpression#toString()
	 */
	@Override
	public String toString() {
		return "(" + leftExpr.toString() + operator.toString() + rightExpr.toString() + ")";
	}
	
	/* (non-Javadoc)
	 * @see modele.metier.NonTerminalExpression#clone()
	 */
	@Override
	public BinaryExpression clone() {
		return new BinaryExpression(this);
	}

	/* (non-Javadoc)
	 * @see modele.metier.NonTerminalExpression#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((leftExpr == null) ? 0 : leftExpr.hashCode());
		result = prime * result + ((operator == null) ? 0 : operator.hashCode());
		result = prime * result + ((rightExpr == null) ? 0 : rightExpr.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see modele.metier.NonTerminalExpression#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BinaryExpression other = (BinaryExpression) obj;
		if (leftExpr == null) {
			if (other.leftExpr != null)
				return false;
		} else if (!leftExpr.equals(other.leftExpr))
			return false;
		if (operator == null) {
			if (other.operator != null)
				return false;
		} else if (!operator.equals(other.operator))
			return false;
		if (rightExpr == null) {
			if (other.rightExpr != null)
				return false;
		} else if (!rightExpr.equals(other.rightExpr))
			return false;
		return true;
	}
}
