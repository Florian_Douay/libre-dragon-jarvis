package modele.technique.parser;

public interface IParserResult{
	
	IOperatorsAndRulesParserResult getOperatorsAndRules();

	IGameLevelsParserResult getGameLevels();

	void setOperatorsAndRules(IOperatorsAndRulesParserResult operatorsAndRules);

	void setGameLevels(IGameLevelsParserResult gameLevels);

}
