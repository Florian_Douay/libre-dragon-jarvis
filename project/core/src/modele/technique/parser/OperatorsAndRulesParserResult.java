package modele.technique.parser;

import java.util.ArrayList;
import java.util.HashMap;

import modele.metier.Operator;
import modele.metier.GlobalRule;
import modele.metier.LocalRule;

public class OperatorsAndRulesParserResult implements IOperatorsAndRulesParserResult {
	
	private HashMap<String,Operator> operators;
	private ArrayList<LocalRule> localRules;
	private ArrayList<GlobalRule> globalRules;

	public OperatorsAndRulesParserResult(HashMap<String,Operator> operators, ArrayList<LocalRule> localRules, ArrayList<GlobalRule> globalRules) {
		this.operators = operators;
		this.localRules = localRules;
		this.globalRules = globalRules;
	}

	@Override
	public HashMap<String,Operator> getOperators() {
		return operators;
	}

	@Override
	public ArrayList<LocalRule> getLocalRules() {
		return localRules;
	}
	
	@Override
	public ArrayList<GlobalRule> getGlobalRules() {
		return globalRules;
	}

}
