package modele.technique.parser;

public class ParserResult implements IParserResult {
	
	private IOperatorsAndRulesParserResult operatorsAndRules;
	private IGameLevelsParserResult gameLevels;
	
	public ParserResult(){
		operatorsAndRules = null;
		gameLevels = null;
	}
	
	@Override
	public void setOperatorsAndRules(IOperatorsAndRulesParserResult operatorsAndRules) {
		this.operatorsAndRules = operatorsAndRules;
	}
	
	@Override
	public void setGameLevels(IGameLevelsParserResult gameLevels) {
		this.gameLevels = gameLevels;
	}

	@Override
	public IOperatorsAndRulesParserResult getOperatorsAndRules() {
		return operatorsAndRules;
	}

	@Override
	public IGameLevelsParserResult getGameLevels() {
		return gameLevels;
	}

}
