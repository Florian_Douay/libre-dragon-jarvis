package modele.technique.parser;

import java.util.ArrayList;
import java.util.HashMap;

import modele.metier.Operator;
import modele.metier.GlobalRule;
import modele.metier.LocalRule;

public interface IOperatorsAndRulesParserResult {
	
	public HashMap<String,Operator> getOperators();

	public ArrayList<LocalRule> getLocalRules();
	
	public ArrayList<GlobalRule> getGlobalRules();

}
