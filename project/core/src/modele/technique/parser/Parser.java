package modele.technique.parser;

import java.io.FileInputStream;
import java.util.HashMap;

import modele.metier.Operator;
import modele.technique.parser.generatedFiles.gameLevels.gameLevelsParser;
import modele.technique.parser.generatedFiles.operatorsAndRules.operatorsAndRulesParser;

public class Parser {
	
	private IParserResult iparserResult;
	private String pathToOperatorConfigFile;
	private String pathToLevelsConfigFile;

	public Parser(String pathToOperatorConfigFile, String pathToLevelsConfigFile) {
		this.iparserResult = null;
		this.pathToOperatorConfigFile = pathToOperatorConfigFile;
		this.pathToLevelsConfigFile = pathToLevelsConfigFile;
	}
	
	public IParserResult getParserResult() {
		return iparserResult;
	}
	
	public boolean parseAll() {
		
		if(iparserResult != null){
			System.err.println("The parsing operation has been already done");
			return false;
		}
		
		iparserResult = new ParserResult();
		
		iparserResult.setOperatorsAndRules(parseOperatorsAndRules());
		
		iparserResult.setGameLevels(parseGameLevels());
		
		return true;
	}
	
	private IOperatorsAndRulesParserResult parseOperatorsAndRules() {
		
		FileInputStream inputStreamOperators = null;
		
		//Opens the file
		try { inputStreamOperators = new FileInputStream(pathToOperatorConfigFile); }
		
	    catch(java.io.FileNotFoundException e){
	    	
	        System.err.println("config file for operators and rules not found at " + pathToOperatorConfigFile);
	        System.err.println("The program end here");
	        System.exit(-1);}	
		
		//Instantiates the parser with the config path
	    operatorsAndRulesParser parser = new operatorsAndRulesParser(inputStreamOperators);
	    
	    IOperatorsAndRulesParserResult parserOpAndRulesResult = null;
	    
	    //Executes the parsing
	    try{ parserOpAndRulesResult = parser.File(); }
	    
	    catch (java.text.ParseException e){
	        System.err.println("Caught java.text.ParseException: " + e.getMessage());
	        System.exit(-1);}
	    catch (modele.technique.parser.generatedFiles.operatorsAndRules.ParseException e){
		    System.err.println("Caught modele.modeleTechnique.parser.generatedFiles.operatorsAndRules.ParseException: " + e.getMessage());
		    System.exit(-1);}
	        
	    return parserOpAndRulesResult;
	}
	
	private IGameLevelsParserResult parseGameLevels() {
		
		if(this.iparserResult.getOperatorsAndRules() == null){
			System.err.println("Can't parser levels before parsing operators and rules");
			return null;
		}
		
		FileInputStream inputStreamOperators = null;
		
		//Opens the file
		try { inputStreamOperators = new FileInputStream(pathToLevelsConfigFile); }
		
	    catch(java.io.FileNotFoundException e){
	        System.err.println("config file for game levels not found at " + pathToLevelsConfigFile);
	        System.err.println("The program end here");
	        System.exit(-1);}	
		
		//Instantiates the parser with the config path
	    gameLevelsParser parser = new gameLevelsParser(inputStreamOperators);
	    
	    //Retrieves operators container needed for the game levels parsing
	    HashMap<String,Operator> operatorHash = this.iparserResult.getOperatorsAndRules().getOperators();
	    
	    IGameLevelsParserResult parserGameLevelsResult = null;
	    
	    //Executes the parsing
	    try{ parserGameLevelsResult = parser.File(operatorHash); }
	    
	    catch (java.text.ParseException e){
	        System.err.println("Caught java.text.ParseException: " + e.getMessage());
	        System.exit(-1);}
	    catch (modele.technique.parser.generatedFiles.gameLevels.ParseException e){
		    System.err.println("Caught modele.modeleTechnique.parser.generatedFiles.operatorsAndRules.ParseException: " + e.getMessage());
		    System.exit(-1);}
	        
	    return parserGameLevelsResult;
	}
	
	public void printParsingResult() {
		
		if(iparserResult == null){
			System.err.println("The parsing operation has'nt been done yet");
			return;
		}
		
		System.out.println("##################################");
		System.out.println("Resultats du parsing");
		System.out.println("");
		
	    //Print result of operators and rules parsing
        System.out.println("Parsing result : the operator container contains " + this.iparserResult.getOperatorsAndRules().getOperators().size() + " operators :");
        System.out.println(this.iparserResult.getOperatorsAndRules().getOperators().values().toString());
        System.out.println("");
        System.out.println("Parsing result : the rule container contains " + this.iparserResult.getOperatorsAndRules().getLocalRules().size() + " rules :");
        System.out.println(this.iparserResult.getOperatorsAndRules().getLocalRules().toString());
        System.out.println("");
        System.out.println("Parsing result : the rule container contains " + this.iparserResult.getOperatorsAndRules().getGlobalRules().size() + " rules :");
        System.out.println(this.iparserResult.getOperatorsAndRules().getGlobalRules().toString());
        System.out.println("");
        
	    //Print result of game levels parsing
        System.out.println("Parsing result : the game levels container contains " + this.getParserResult().getGameLevels().getGameLevels().size() + " levels :");
        System.out.println(this.getParserResult().getGameLevels().getGameLevels().toString());
        
        System.out.println("");
        System.out.println("Fin des resultats du parsing");
        System.out.println("##################################");
	}
}
