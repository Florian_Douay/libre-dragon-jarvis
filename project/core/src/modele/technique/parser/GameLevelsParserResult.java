package modele.technique.parser;

import java.util.ArrayList;

import modele.metier.GameLevel;

public class GameLevelsParserResult implements IGameLevelsParserResult {

	private ArrayList<GameLevel> gameLevels;

	public GameLevelsParserResult(ArrayList<GameLevel> rules) {
		this.gameLevels = rules;
	}

	@Override
	public ArrayList<GameLevel> getGameLevels() {
		return gameLevels;
	}
}
