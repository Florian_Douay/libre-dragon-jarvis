package modele.technique.parser;

import java.util.ArrayList;

import modele.metier.GameLevel;

public interface IGameLevelsParserResult {
	
	public ArrayList<GameLevel> getGameLevels();

}
