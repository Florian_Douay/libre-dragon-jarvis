package modele.technique;

import java.util.ArrayList;
import java.util.HashMap;

import modele.metier.GameLevel;
import modele.metier.Operator;
import modele.metier.LocalRule;
import modele.metier.GlobalRule;
import modele.technique.parser.ParserResult;

public class ModelImplementer {
	
	ParserResult parserResult;
	
	public ModelImplementer(ParserResult parserResult){
		
		this.parserResult = parserResult;
	}
	
	public HashMap<String,Operator> getOperatorMap() {
		
		HashMap<String,Operator> operatorHash = parserResult.getOperatorsAndRules().getOperators();
		return operatorHash;
	}
	
	public HashMap<Integer,LocalRule> getLocalRuleArray() {

		ArrayList<LocalRule> localRuleArrayList = parserResult.getOperatorsAndRules().getLocalRules();
		
		HashMap<Integer,LocalRule> localRuleMap = new HashMap<Integer,LocalRule>();
		
		for (LocalRule localRule : localRuleArrayList) {
			
			localRuleMap.put(localRule.getRuleId(), localRule);
		}
		
		return localRuleMap;
	}
	
	public HashMap<Integer,GlobalRule> getGlobalRuleArray() {

		ArrayList<GlobalRule> gloabalRuleArrayList = parserResult.getOperatorsAndRules().getGlobalRules();
		
		HashMap<Integer,GlobalRule> globalRuleMap = new HashMap<Integer,GlobalRule>();
		
		for (GlobalRule globalRule : gloabalRuleArrayList) {
			
			globalRuleMap.put(globalRule.getRuleId(), globalRule);
		}
		
		return globalRuleMap;
	}
	
	public ArrayList<GameLevel> getGameLevelArray() {
		
		ArrayList<GameLevel> levelArrayList = parserResult.getGameLevels().getGameLevels();
		return levelArrayList;
	}
	
}
