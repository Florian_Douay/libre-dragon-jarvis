package com.libredragonjarvis.view;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;

import com.badlogic.gdx.Game;

import managers.GameManager;

/**
 * Game class which extends from com.badlogic.gdx.Game
 * <p>
 * It allows : 
 * <ul>
 * <li>To initializes the system with given configuration files paths</li>
 * <li>To set the game to show the MainMenuScreen</li>
 * </ul>
 * 
 * @since 1.0
 * @see GameManager
 * @see MainMenuScreen
 */

public class LibreDragonJarvis extends Game {
	
	/**
	 * Initializes the system.
	 * Set the config_files path and set the MainMenuScreen
	 */
	@Override
	public void create () {
		
		String pathToOperatorConfigFile = "../ressources/config_files/operators_and_rules/operatorsAndRules_01.txt";
		String pathToLevelsConfigFile = "../ressources/config_files/game_levels/gameLevels_01.txt";
		
		File file = new File("log_err.txt");
        try {
            PrintStream printStream = new PrintStream(file);
            System.setErr(printStream);
            System.err.println("Application Starting...");
            System.err.println("Error stream ON");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
		
		// Program initialization
		if (GameManager.getInstance().init(pathToOperatorConfigFile,pathToLevelsConfigFile) == false) {
			System.out.println("Failed to initialize the program");
			System.out.println("Program exit");
			System.exit(-1);
		}
		setScreen(new MainMenuScreen(this));
	}

}