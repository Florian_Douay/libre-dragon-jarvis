package com.libredragonjarvis.view;

import managers.GameLevelManager;
import managers.GameManager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane.ScrollPaneStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.utils.viewport.FitViewport;

/**
 * MainMenuScreen which load the level selected by the user
 * And then switch to the GameScreen
 * 
 * @since 1.0
 * @see GameScreen
 * @see LibreDragonJarvis
 */

public class MainMenuScreen implements Screen{
	
	/**
	 * The game instance
	 */
	private final LibreDragonJarvis game;
	
	/**
	 * The stage which contains the GUI elements
	 */
	private Stage stage;
	
	/**
	 * Allows to know if a level is loaded by the GameLevelManager
	 */
	private boolean levelLoaded = false;
	
	/**
	 * MainMenuScreen constructor
	 * @param the game instance
	 */
	MainMenuScreen(LibreDragonJarvis gam)
	{
		this.game = gam;
		
		//create the stage
		this.stage = new Stage(new FitViewport(1600, 900));
		stage.setDebugAll(false); //set to true to see the debug line
		
		//Generate the font we will use
		BitmapFont font = new BitmapFont();
		FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/Dimbo Regular.ttf"));
		FreeTypeFontParameter parameter = new FreeTypeFontParameter();
		
		//Set the parameter of the font 
		parameter.size = 28;
		parameter.color = Color.valueOf("00ACC1");
		parameter.borderWidth = 2;
		parameter.borderColor = Color.valueOf("455A64");
		font = generator.generateFont(parameter);
		
		//Generate the button style
		TextButtonStyle style = new TextButtonStyle();
		
		//set the texture and font of the text button
		style.up = new NinePatchDrawable(new NinePatch(new Texture(Gdx.files.internal("buttonUP.png")), 18, 18, 18, 18));
		style.down = new NinePatchDrawable(new NinePatch(new Texture(Gdx.files.internal("buttonDOWN.png")), 18, 18, 18, 18));
		style.font = font;
		style.unpressedOffsetY = 6;
		
		//set the background of MainMenuScreen
		Image background = new Image(new Texture(Gdx.files.internal("Background.png")));
		stage.addActor(background);
		
		//set the Title 
		Image logo = new Image(new Texture(Gdx.files.internal("Logo.png")));
		logo.setPosition(0, 0);
		stage.addActor(logo);
		
		//set the label displaying the currently loaded level
		LabelStyle labelStyle = new LabelStyle(font, font.getColor());
		final Label loadedLevel = new Label("Niveau selectionné : aucun", labelStyle);
		
		//create the table which will contains the button and the scrollpane
		Table table = new Table();
		table.setFillParent(true);
		table.defaults().space(40);
		table.pad(40);
		table.bottom();
		stage.addActor(table);
		
		//create the play button
		TextButton playButton = new TextButton("Play", style);
		playButton.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				if(levelLoaded == true)
				{
					game.setScreen(new GameScreen(game));
				}
			}
		});
		
		//create the quit button
		TextButton quitButton = new TextButton("Quit", style);
		quitButton.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				Gdx.app.exit();
			}
		});
		
		//create the table contained in the scrollpane 
		Table table2 = new Table();
		ScrollPane pane = new ScrollPane(table2);
		ScrollPaneStyle paneStyle = new ScrollPaneStyle();
		
		table2.center();
		table2.defaults().space(10);
		table2.pad(10);
		
		//get the all the levels and create a button for each
		String[][] gameLevels = GameManager.getInstance().getAllGameLevelsStrings();
		for(int i=0; i < gameLevels.length; i++) {
			
			//parse the level formula
			String tmp = new String();
			for(int j=1; j < gameLevels[i][1].length()-1; j++)
			{
				tmp += gameLevels[i][1].charAt(j);
			}
			String tmp2[] = tmp.split(",");
			tmp = new String();
			for(int j=0; j < tmp2.length; j++)
			{
				tmp += tmp2[j];
			}
			tmp = tmp.replace('#', '=');
			
			//create the button
			final TextButton button = new TextButton(tmp, style);
			final String levelToLoad = gameLevels[i][0];
			button.addListener(new ClickListener(){
				@Override
				public void clicked(InputEvent event, float x, float y)
				{
					int levelToLoadID = Integer.parseInt(levelToLoad);
					if (GameLevelManager.getInstance().loadGameLevelById(levelToLoadID) == false) {
					}
					else
					{
						levelLoaded = true;
						loadedLevel.setText("Niveau selectionné : "+button.getLabel().getText().toString());
					}
				}
			});
			table2.add(button).center().pad(10);
			table2.row();
		}
		
		//create the style of the scrollpane
		paneStyle.background = new NinePatchDrawable(new NinePatch(new Texture(Gdx.files.internal("paneBack.png")), 6, 6, 6, 6));
		paneStyle.vScrollKnob = new NinePatchDrawable(new NinePatch(new Texture(Gdx.files.internal("paneKnob.png")), 14, 14, 14, 14));;
		paneStyle.hScrollKnob = new NinePatchDrawable(new NinePatch(new Texture(Gdx.files.internal("paneKnob.png")), 14, 14, 14, 14));;
		
		//set the scrollpane parameters
		pane.setStyle(paneStyle);;
		pane.setOverscroll(false, false);
		pane.setScrollBarPositions(true, true);
		pane.setFadeScrollBars(true);
		pane.setScrollbarsOnTop(true);
		pane.setVariableSizeKnobs(true);
		pane.setScrollingDisabled(false, false);
		
		//add the elements to the table
		table.add(loadedLevel).center();
		table.row();
		table.add(pane).height(300).width(600);
		table.row();
		table.add(playButton);	
		table.row();
		table.add(quitButton);
	}
	
	/**
	 * Function called when the screen is shown
	 */
	@Override
	public void show() {
		Gdx.input.setInputProcessor(stage);
	}
	
	/**
	 * Renderer the element of the stage
	 */
	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0.1f, 0.1f, 0.1f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		stage.act(delta);
		stage.draw();
	}
	
	/**
	 * Adapt the stage to window size
	 */
	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height, false);
	}

	/**
	 * Function not used 
	 */
	@Override
	public void pause() {}

	/**
	 * Function not used 
	 */
	@Override
	public void resume() {}

	/**
	 * Function not used 
	 */
	@Override
	public void hide() {}

	/**
	 * Free the ressources used by the stage
	 */
	@Override
	public void dispose() {
		Gdx.input.setInputProcessor(null);
		stage.dispose();
	}
}
