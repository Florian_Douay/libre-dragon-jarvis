package com.libredragonjarvis.view;

import java.util.Observable;
import java.util.Observer;

import managers.GameLevelManager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane.ScrollPaneStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Window.WindowStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;

/**
 * LevelRenderer
 * Create the game component and display them
 * + is an observer for the models change
 * @see Observer
 * @since 1.0
 */
public class LevelRenderer implements Observer {
	
	/**
	 * The game instance
	 */
	private LibreDragonJarvis game;
	
	/**
	 * Number of unit a level is wide
	 */
	private int Level_WIDTH = 24;
	
	/**
	 * Number of unit a level is high
	 */
	private int Level_HEIGHT = 13;
	
	/**
	 * The current level
	 */
	private Level level;
	
	/**
	 * The listener for the current level
	 */
	private LevelController controller;

	/**
	 * Stage container for the game component
	 */
	private Stage stage;
	
	/**
	 * Texture for the pane background
	 */
	private NinePatchDrawable paneBackTexture;
	
	/**
	 * Texture for Stage background
	 */
	private Texture backgroundTexture;
	
	/**
	 * Button style for every other button
	 */
	private TextButtonStyle style;
	
	/**
	 * Button style for the equation block
	 */
	private TextButtonStyle style2;
	
	/**
	 * Font generator
	 */
	private FreeTypeFontGenerator generator;
	
	/**
	 * Font parameter, such as color, outline, etc..
	 */
	private FreeTypeFontParameter parameter;
	
	/**
	 * Font instance
	 */
	private BitmapFont font;
	
	/**
	 * Texture for the pane background, knob, etc..
	 */
	private ScrollPaneStyle paneStyle;
	
	/**
	 * Screen width
	 */
	private float width;
	
	/**
	 * Screen height
	 */
	private float height;
	
	/**
	 * Pixel per Unit on the X axis
	 */
	private float ppuX;
	
	/**
	 * Pixel per Unit on the Y axis
	 */
	private float ppuY;
	
	/**
	 * Get the stage for the current level
	 * @return the current stage 
	 */
	public Stage getStage() { return this.stage; }
	
	/**
	 * Get the renderer font
	 * @return the font used by the renderer
	 */
	public BitmapFont getFont() { return this.font; }
	
	/**
	 * Get the style of the button 
	 * @return the TextButtonStyle used by the renderer
	 */
	public TextButtonStyle getButtonStyle() { return this.style; }
	
	/**
	 * Get the Texture of the pane background 
	 * @return the texture of the pane background used by the renderer
	 */
	public Drawable getHolderTexture() { return paneBackTexture; }
	
	/**
	 * LevelRenderer constructor
	 * @param gam The game instance
	 * @param level current level
	 * @param debug set the debug line for the stage
	 * @param width screen width
	 * @param height screen height
	 */
	LevelRenderer(LibreDragonJarvis gam, Level level, boolean debug, float width, float height)
	{
		this.game = gam;
		this.level = level;
		this.width = width;
		this.height = height;
		this.stage = new Stage(new FitViewport(width, height));
		this.stage.setDebugAll(debug);
		this.ppuX = this.width / Level_WIDTH;
		this.ppuY = this.height / Level_HEIGHT;
		loadTextures();
		createLayout();
		this.controller = new LevelController(this.game, this.level, this);
	}
	
	/**
	 * Subscribe the levelRenderer as a model observer 
	 */
	public void subscribeInstanceAsModelObserver()
	{
		GameLevelManager.getInstance().addObserverToLevelsModel(this);
	}
	
	/**
	 * Modify the ppuX and ppuY according to the new screen size
	 * @param width new sreen width
	 * @param height new screen height
	 */
	public void resize(int width, int height)
	{
		this.width = (float)width;
		this.height = (float)height;
		this.ppuX = this.width / Level_WIDTH;
		this.ppuY = this.height / Level_HEIGHT;
	}
	
	/**
	 * Load every image needed to be renderer
	 */
	private void loadTextures()
	{
		paneBackTexture = new NinePatchDrawable(new NinePatch(new Texture(Gdx.files.internal("paneBack.png")), 6, 6, 6, 6));
		backgroundTexture = new Texture(Gdx.files.internal("Background.png"));
		
		generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/Dimbo Regular.ttf"));
		parameter = new FreeTypeFontParameter();
		font = new BitmapFont();
		
		parameter.size = 28;
		parameter.color = Color.valueOf("00ACC1");
		parameter.borderWidth = 2;
		parameter.borderColor = Color.valueOf("455A64");
		font = generator.generateFont(parameter);
		
		style = new TextButtonStyle();
		style.up = new NinePatchDrawable(new NinePatch(new Texture(Gdx.files.internal("buttonUP.png")), 18, 18, 18, 18));
		style.down = new NinePatchDrawable(new NinePatch(new Texture(Gdx.files.internal("buttonDOWN.png")), 18, 18, 18, 18));
		style.font = font;
		style.unpressedOffsetY = 8;
		
		style2 = new TextButtonStyle();
		style2.up = new NinePatchDrawable(new NinePatch(new Texture(Gdx.files.internal("Block.png")), 18, 18, 18, 18));
		// TODO style.down = new NinePatchDrawable(new NinePatch(new Texture(Gdx.files.internal("buttonDOWN.png")), 18, 18, 18, 18));
		style2.font = font;
		style2.unpressedOffsetY = 8;
		
		paneStyle = new ScrollPaneStyle();
		paneStyle.background = paneBackTexture;
		paneStyle.vScrollKnob = new NinePatchDrawable(new NinePatch(new Texture(Gdx.files.internal("paneKnob.png")), 14, 14, 14, 14));;
		paneStyle.hScrollKnob = new NinePatchDrawable(new NinePatch(new Texture(Gdx.files.internal("paneKnob.png")), 14, 14, 14, 14));;
	}
	
	/**
	 * Set the ScrollPane properties
	 * @param pane the one to be modified
	 * @param rect bounds for the pane
	 */
	public void setPaneProperties(ScrollPane pane, Rectangle rect)
	{
		pane.setStyle(paneStyle);
		pane.setBounds(rect.x, rect.y, rect.width, rect.height);
		pane.setStyle(paneStyle);;
		pane.setOverscroll(false, false);
		pane.setScrollBarPositions(true, true);
		pane.setFadeScrollBars(true);
		pane.setScrollbarsOnTop(false);
		pane.setVariableSizeKnobs(true);
		pane.setScrollingDisabled(false, true);
	}
	
	/**
	 * Draw the stage to the screen
	 * @param delta the time span between the current frame and the last frame in seconds.
	 */
	public void render(float delta)
	{
		this.stage.act(delta);
		this.stage.draw();
	}
	
	/**
	 * Free the ressources used by the stage
	 */
	public void dispose()
	{
		stage.dispose();
	}
	
	/**
	 * Call every function that create the layout component of the stage
	 */
	private void createLayout()
	{
		stage.addActor(new Image(backgroundTexture));
		createEquationLeft();
		createEquationRight();
		createOperators();
		createPile();
		createButtons();
		createApplyZone();
		createPopup();
	}
	
	/**
	 * Create the left equation layout
	 */
	private void createEquationLeft()
	{
		Table table = new Table();
		ScrollPane pane = new ScrollPane(table);
		Rectangle rect = new Rectangle();
		
		rect.set(ppuX, 9*ppuY+25, 22*ppuX, ppuY+50);
		setPaneProperties(pane, rect);
		table.left();
		table.defaults().space(10);
		
		for(int i = 0; i < level.getEquationLeft().size; i++)
		{
			level.getEquationLeft().get(i).setStyle(style2);
			table.add(level.getEquationLeft().get(i)).expandY().width(50);
		}
		pane.setName("EquationLeft");
		stage.addActor(pane);
	}
	
	/**
	 * Create the right equation layout
	 */
	private void createEquationRight()
	{
		Table table = new Table();
		ScrollPane pane = new ScrollPane(table);
		Rectangle rect = new Rectangle();
		
		rect.set(ppuX, 6*ppuY, 22*ppuX, ppuY+50);
		setPaneProperties(pane, rect);
		table.left();
		table.defaults().space(10);
		
		for(int i = 0; i < level.getEquationRight().size; i++)
		{
			level.getEquationRight().get(i).setStyle(style2);
			table.add(level.getEquationRight().get(i)).expandY().width(50);
		}
		pane.setName("EquationRight");
		stage.addActor(pane);
	}
	
	/**
	 * Create the pile layout
	 */
	private void createPile()
	{
		Table table = new Table();
		ScrollPane pane = new ScrollPane(table);
		Rectangle rect = new Rectangle();
		
		rect.set(16*ppuX, ppuY, 7*ppuX, 4*ppuY);
		setPaneProperties(pane, rect);
		
		table.left().top().pad(10);
		for(int i = 0; i < level.getPile().size; i++)
		{
			level.getPile().get(i).setStyle(style);
			table.add(level.getPile().get(i)).align(Align.left).space(10);
			if((i+1)%7 == 0)
			{
				table.row();
			}
		}
		pane.setName("Pile");
		stage.addActor(pane);
	}
	
	/**
	 * Create the operators pane layout
	 */
	private void createOperators()
	{
		Table table = new Table();
		ScrollPane pane = new ScrollPane(table);
		Rectangle rect = new Rectangle();
		
		rect.set(ppuX, ppuY, 7*ppuX, 4*ppuY);
		setPaneProperties(pane, rect);
		
		table.left().top().pad(10);
		for(int i=0; i < level.getOperator().size; i++)
		{
			level.getOperator().get(i).setStyle(style);
			table.add(level.getOperator().get(i)).align(Align.left).space(10);
			if((i+1)%7 == 0)
			{
				table.row();
			}
		}
		pane.setName("Operators");
		stage.addActor(pane);
	}
	
	/**
	 * Create every buttons
	 */
	private void createButtons()
	{
		TextButton quitButton = new TextButton("Retour au menu principal", style);
		quitButton.setPosition(14*ppuX, 12*ppuY-20);
		quitButton.setName("Quit");
		
		TextButton reloadButton = new TextButton("Relancer la partie", style);
		reloadButton.setPosition(6*ppuX, 12*ppuY-20);
		reloadButton.setName("Reload");
		
		TextButton equalButton = new TextButton("=", style);
		equalButton.setPosition(11*ppuX, 8*ppuY);
		equalButton.setName("Equal");
		
		TextButton applyButton = new TextButton("Appliquer", style);
		applyButton.setPosition(10*ppuX-2, 2*ppuY-20);
		applyButton.setName("Apply");
		
		TextButton clearButton = new TextButton("Effacer", style);
		clearButton.setPosition(12*ppuX + 12, 2*ppuY-20);
		clearButton.setName("Clear");
		
		stage.addActor(quitButton);
		stage.addActor(reloadButton);
		stage.addActor(equalButton);
		stage.addActor(applyButton);
		stage.addActor(clearButton);
	}
	
	/**
	 * Create the "ApplyZone" layout,
	 * the place where an operator and a block form the pile
	 * must be placed
	 */
	private void createApplyZone()
	{
		Table table = new Table();

		table.setBounds(10*ppuX, 3*ppuY, 4*ppuX, ppuY+50);
		table.setBackground(paneBackTexture);
		
		for(int i=0; i < level.getApplyZone().size; i++)
		{
			level.getApplyZone().get(i).setStyle(style2);
			level.getApplyZone().get(i).setVisible(false);
			table.add(level.getApplyZone().get(i)).expand().space(10);
		}
		table.setName("ApplyZone");
		stage.addActor(table);
	}
	
	/**
	 * Create the popup and add it to the stage as hidden
	 */
	private void createPopup()
	{
		level.setPopup(new Popup(this));
		stage.addActor(level.getPopup().getWindow());
	}
	
	/**
	 * Create the dialog shown when a level is won 
	 */
	private void createWonDialog()
	{
		WindowStyle style = new WindowStyle();
		style.background = paneBackTexture;
		style.titleFont = font;
		
		final Window dialog = new Window("", style);
		
		parameter.size = 43;
		parameter.color = Color.valueOf("00ACC1");
		parameter.borderWidth = 2;
		parameter.borderColor = Color.valueOf("455A64");
		BitmapFont font = generator.generateFont(parameter);
		
		LabelStyle labelStyle = new LabelStyle(font, font.getColor());
		Label label = new Label("Niveau \nReussi", labelStyle);

		TextButton button1 = new TextButton("Recommencer", this.style);
		button1.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				GameLevelManager.getInstance().restartCurrentGameLevel();
				game.setScreen(new GameScreen(game));
				dialog.setVisible(false);
				dialog.remove();
			}
		});
		
		TextButton button2 = new TextButton("Retour au menu principal", this.style);
		button2.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				GameLevelManager.getInstance().restartCurrentGameLevel();
				GameLevelManager.getInstance().unloadCurrentGameLevel();
				game.setScreen(new MainMenuScreen(game));
				dialog.setVisible(false);
				dialog.remove();
			}
		});
		
		dialog.add(label).center().top().pad(10).expand();
		dialog.row();
		dialog.add(button1).center().top().pad(10);
		dialog.row();
		dialog.add(button2).center().top().pad(10);
		dialog.setSize(400, 400);
		dialog.setPosition(stage.getWidth()-(stage.getWidth()/2)-dialog.getWidth()/2, stage.getHeight()/3);
		dialog.top().center();
		dialog.setModal(true);
		
		stage.addActor(dialog);
	}
	
	/**
	 * Update method for the Observer pattern
	 */
	@Override
	public void update(Observable arg0, Object arg1) {
		level.updateLevelContent();
		stage.clear();
		createLayout();
		controller = new LevelController(game, level, this);
		if(GameLevelManager.getInstance().isTheCurrentGameLevelWon())
		{
			createWonDialog();
		}
	}
}
