package com.libredragonjarvis.view;

import managers.RulesApplicationManager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.ui.Window.WindowStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

/**
 * Popup which contain the local or global rules
 * according to the block clicked
 * 
 * @since 1.0
 */
public class Popup {
	
	/**
	 * The Popup
	 */
	private Window popup;
	
	/**
	 * The renderer to access some ressources
	 */
	private LevelRenderer renderer;
	
	/**
	 * Get the popup
	 * @return Window
	 */
	public Window getWindow() { return this.popup; }
	
	/**
	 * Set the window of the popup
	 * @param window
	 */
	public void setWindow(Window window) { this.popup = window; }
	
	/**
	 * Popup constructor
	 * @param renderer
	 */
	Popup(LevelRenderer renderer)
	{
		this.renderer = renderer;
		WindowStyle style = new WindowStyle();
		
		//get the window style from the renderer
		style.titleFont = renderer.getFont();
		style.background = renderer.getHolderTexture();
		
		//set the window parameter 
		this.popup = new Window("Operations", style);
		this.popup.setName("Popup");
		this.popup.setVisible(false);
		this.popup.setModal(true);
	}
	
	/**
	 * Update the content of the window according to block clicked
	 * @param operations list of the possible operations
	 * @param ID ID of the block clicked
	 * @param value Value of the block clicked
	 */
	public void updateTable(String[][] operations, String ID, String value)
	{
		popup.clear();
		popup.top().left();
		popup.setWidth(1000);
		popup.setHeight(400);
		
		//set the title of the popup with the block click value
		popup.getTitleLabel().setText("Operations pour : ' "+value+" '");
		
		//create the close button style
		ButtonStyle style = new ButtonStyle();
		style.up = new TextureRegionDrawable(new TextureRegion(new Texture(Gdx.files.internal("buttonCloseUP.png"))));
		style.down = new TextureRegionDrawable(new TextureRegion(new Texture(Gdx.files.internal("buttonCloseDOWN.png"))));
		
		//create the close button
		Button cancel = new Button(style);
		cancel.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				popup.setVisible(false);
			}
		});
		popup.add(cancel).right().top().pad(10).size(50);
		popup.row();
		
		//create the table which will contain the list of possible operations
		final String blockID = ID;
		Table table = new Table();
		for(int i=0; i < operations.length; i++)
		{
			operations[i][1] = operations[i][1].replaceAll("##", "<=>");
			operations[i][1] = operations[i][1].replace('#', '='); 
			final Block operation = new Block(operations[i][0], operations[i][1]);
			operation.setStyle(renderer.getButtonStyle());
			operation.addListener(new ClickListener(){
				@Override
				public void clicked(InputEvent event, float x, float y)
				{
					if(blockID == "2")
					{
						RulesApplicationManager.getInstance().applyGlobalRule(Integer.parseInt(operation.getID()));
					}
					else
					{
						RulesApplicationManager.getInstance().applyLocalRule(Integer.parseInt(operation.getID()), blockID);
					}
					popup.setVisible(false);
				}
			});
			table.add(operation).expand().center().pad(10);
			table.row();
		}
		
		//create the scrollpane that contain the operations list table
		ScrollPane pane = new ScrollPane(table);
		renderer.setPaneProperties(pane, new Rectangle());
		pane.setScrollingDisabled(false, false);
		pane.setFadeScrollBars(false);
		
		popup.add(pane).size(990, 320);
	}
}
