package com.libredragonjarvis.view.CmdLine;


import java.util.Scanner;

import managers.GameLevelManager;
import managers.GameManager;
import managers.RulesApplicationManager;

public class CmdLine {
	
	public static void main (String[] args)
	{
		
		String pathToOperatorConfigFile = "../ressources/config_files/operators_and_rules/operatorsAndRules_01.txt";
		String pathToLevelsConfigFile = "../ressources/config_files/game_levels/gameLevels_01.txt";
		
		boolean isSuccess;
		boolean stop = false;
		Scanner reader = new Scanner (System.in);
		String[] currentLevel;
		String[][] allLevels;
		String ask;
		
		GameManager.getInstance().init(pathToOperatorConfigFile, pathToLevelsConfigFile);
		
		/* External loop (to play another level) */
		do
		{
			/* print all levels */
			allLevels = GameManager.getInstance().getAllGameLevelsStrings();
			System.out.println ("ALL LEVELS:");
			for (int i = 0; i < allLevels.length; i++)
			{
				for (int j = 0; j < allLevels[i].length; j++)
				{
					System.out.println (allLevels[i][j]);
				}
				
				System.out.println ("\n");
			}
			
			/* choice of current level */
			System.out.println ("Which level do you want to try ?");
			ask = reader.nextLine();
			GameLevelManager.getInstance().loadGameLevelById(Integer.parseInt(ask));
			currentLevel = GameLevelManager.getInstance().getCurrentGameLevelStrings();
			
			/* print current level */
			System.out.println ("LEVEL " + ask + " CHOOSEN:");
			for(int i = 0; i < currentLevel.length; i++)
			{			
				System.out.println (currentLevel[i]);
			}
			
			/* internal loop */
			do
			{
				System.out.println ("\nWhat do you want to do ?\n" +
						"lr : local rule\n" +
						"gr : global rule\n" +
						"st : stack\n" +
						"q  : quit current level");
					
				/* read stdin */
				System.out.print ("Enter your choice:");
				ask = reader.nextLine ();
				
				/* local rules */
				if (ask.compareTo("lr") == 0)
				{
					System.out.println ("\nLOCAL RULES: ");
					System.out.print ("Enter the code of the symbole where you want to apply a rule:");
					ask = reader.nextLine ();
					
					/* print all local rules */
					String modelPath = ask;
					String[][] lr = RulesApplicationManager.getInstance().getPossibleFormChanges(ask);
					
					/* do it if there's some local rules */
					if (lr.length != 0)
					{
						for (int i = 0; i < lr.length; i++)
						{
							for (int j = 0; j < lr[i].length; j++)
							{
								System.out.println (lr[i][j]);
							}
						}
							
						/* read stdin */
						System.out.println("Which rule do you want to use ?");
						System.out.print ("Enter your choice:");
						ask = reader.nextLine();
						
						/* apply */
						isSuccess = RulesApplicationManager.getInstance().applyLocalRule(Integer.parseInt(ask), modelPath);
						if (isSuccess)
							System.out.println ("The rule have been applied on the equation.");
						else 
							System.out.println ("Error: The rule have not been applied.");
					}
					else
					{
						System.out.println ("No local rules for this equation.");
					}
				}
				/* global rules */
				else if (ask.compareTo("gr") == 0)
				{
					System.out.println ("\nGLOBAL RULES: ");
					String[][] gr = RulesApplicationManager.getInstance().getPossibleGlobalFormChanges();
					
					/* do it if there's some global rules */
					if (gr.length != 0)
					{
						/* print all global rules */
						for (int i = 0; i < gr.length; i++)
						{
							for (int j = 0; j < gr[i].length; j++)
							{
								System.out.println (gr[i][j]);
							}
						}
							
						/* read stdin */
						System.out.println ("Choose a rule to apply:");
						ask = reader.nextLine();
						
						/* apply */
						isSuccess = RulesApplicationManager.getInstance().applyGlobalRule(Integer.parseInt(ask));
						if (isSuccess)
							System.out.println ("The rule have been applied on the equation.");
						else 
							System.out.println ("Error: The rule have not been applied.");
					}
					/* default */
					else
					{
						System.out.println ("No global rules for this equation.");
					}
				}
				/* stack */
				else if (ask.compareTo("st") == 0)
				{
					System.out.println ("\nSTACK:");
					System.out.println ("With which operator do you want to interact:");
					ask = reader.nextLine();
					String operator = ask;
					int arity = GameManager.getInstance().getOperatorArity(operator);
						
					/* check arity of operator choosen */
					if (arity == 1)
					{
						/* apply */
						isSuccess = RulesApplicationManager.getInstance().applyOperation(operator, null);
						if (isSuccess)
							System.out.println ("The object have been applied to the equation.");
						else
							System.out.println ("The object have not been applied to the equation.");
					}
					else if (arity == 2)
					{
						System.out.println ("With which variable do you want to interact:");
						ask = reader.nextLine();
						String[] argTest = new String[arity-1];
						argTest[0] = ask;
							
						/* apply */
						isSuccess = RulesApplicationManager.getInstance().applyOperation(operator, argTest);
						if (isSuccess)
							System.out.println ("The object have been applied to the equation.");
						else
							System.out.println ("The object have not been applied to the equation.");
					}
					else
						System.out.println ("Error.");
				}
				/* quit current level */
				else if (ask.compareTo("q") == 0)
				{
					stop = true;
				}
				/* default */
				else
				{
					System.out.println ("Bad argument. Retry !");
				}
				
				/* if we want to stop, it's unnecessary to print the result of action */
				if (stop == false)
				{
					System.out.println("\nRESULT:");
					String [] res = GameLevelManager.getInstance().getCurrentGameLevelStrings();
					for (int i = 0; i < res.length; i++)
					{
						System.out.println (res[i]);
					}
				}
			}
			/* While current level is not won */
			while ((GameLevelManager.getInstance().isTheCurrentGameLevelWon() == false) && (stop == false));
			
			/* check victory to print congratulation message */
			if (GameLevelManager.getInstance().isTheCurrentGameLevelWon())
			{
				System.out.println ("Victory !\n");
			}
			
			/* ask to replay */ 
			System.out.println ("Do you want to try another lvl ? [Y/N]");
			ask = reader.nextLine();
			stop = false;
		} 
		/* while we want to play */
		while (ask.compareTo("Y") == 0);
		
		/* close the scanner */
		reader.close();
	}
}
