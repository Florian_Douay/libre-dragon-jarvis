package com.libredragonjarvis.view;

import managers.GameLevelManager;
import com.badlogic.gdx.utils.Array;

/**
 * Level class 
 * Store the data of the equation, operators, pile, operations and the popup
 * as TextButton 
 *
 * @since 1.0
 */
public class Level {
	
	/**
	 * Left side of the equation
	 */
	private Array<Block> equationLeft = new Array<Block>();
	
	/**
	 * Right side of the equation
	 */
	private Array<Block> equationRight = new Array<Block>();
	
	/**
	 * List of operators
	 */
	private Array<Block> operator = new Array<Block>();
	
	/**
	 * Pile of block 
	 */
	private Array<Block> pile = new Array<Block>();
	
	/**
	 * Element of the apply zone (an operator and an value of the pile)
	 */
	private Array<Block> applyZone = new Array<Block>();
	
	/**
	 * list of the operations
	 */
	private Array<Block> operations = new Array<Block>();
	
	/**
	 * The popup for the operations list
	 */
	private Popup popup;
		
	/**
	 * Get the left side of the equation
	 * @return an array of block related to the equation left side
	 */
	public Array<Block> getEquationLeft() {
		return equationLeft;
	}
	
	/**
	 * Get the right side of the equation
	 * @return an array of block related to the equation right side
	 */
	public Array<Block> getEquationRight() {
		return equationRight;
	}

	/**
	 * Get the operator elements
	 * @return an array of block related to the operators
	 */
	public Array<Block> getOperator() {
		return operator;
	}

	/**
	 * Get the pile elements
	 * @return an array of block related to the pile elements
	 */
	public Array<Block> getPile() {
		return pile;
	}
	
	/**
	 * Get the elements in the apply zone
	 * @return an array of block related to the elements that are in the apply zone
	 */
	public Array<Block> getApplyZone() {
		return applyZone;
	}
	
	/**
	 * Get the operations possible
	 * @return an array of block related to the possible operations
	 */
	public Array<Block> getOperations() {
		return operations;
	}
	
	/**
	 * Get the content of the popup
	 * @return the popup
	 */
	public Popup getPopup() {
		return popup;
	}
	
	/**
	 * Set the popup 
	 * @param popup
	 */
	public void setPopup(Popup popup) {
		this.popup = popup;
	}
	
	/**
	 * Level constructor
	 */
	Level()
	{
		this.popup = null;
		this.updateLevelContent();
	}
	
	/**
	 * Update the list of possible operations
	 * @param operations the list of newly possible operations
	 */
	public void updateOperations(String[][] operations)
	{
		this.operations.clear();
		
		for(int i=0; i < operations.length; i++)
		{
			this.operations.add(new Block(operations[i][0], operations[i][1]));
		}
	}
	
	/**
	 * Create the array of blocks for the equation
	 * @param formula the equation
	 * @param ID IDs of every char in the equation
	 */
	private void createEquation(String formula, String ID)
	{
		//remove '[' and ']' from the strings
		String tmpID = new String();
		String tmpValue = new String();
		for(int i=1; i< ID.length()-1; i++)
		{
			tmpID += ID.charAt(i);
		}
		for(int i=1; i< formula.length()-1; i++)
		{
			tmpValue += formula.charAt(i);
		}
		
		//isolate every variable and id
		String s[] = tmpID.split(", ");
		String f[] = tmpValue.split(",");

		int id = 0;
		for(int i=0; i < f.length; i++)
		{
			if(f[i].compareTo("#") == 0)
			{
				id = i;
				break;
			}
			this.equationLeft.add(new Block(s[i], f[i]));
		}
		for(int i=id+1; i < f.length; i++)
		{
			this.equationRight.add(new Block(s[i], f[i]));
		}
	}
	
	/**
	 * Create the array of blocks for the pile
	 * @param pile the list of elements in the pile
	 */
	private void createPile(String pile)
	{
		for(int i=1; i< pile.length()-1; i++)
		{
			if(pile.charAt(i) == ',')
			{
				
			}
			else if (pile.charAt(i) == ' ')
			{
				
			}
			else
			{
				this.pile.add(new Block(null, ""+pile.charAt(i)));
			}
		}	
	}
	
	/**
	 * Create the array of blocks for the operators
	 * @param operators the list of operators
	 */
	private void createOperators(String operators)
	{
		for(int i=1; i< operators.length()-1; i++)
		{
			if(operators.charAt(i) == ',')
			{
				
			}
			else if (operators.charAt(i) == ' ')
			{
				
			}
			else
			{
				this.operator.add(new Block(null, ""+operators.charAt(i)));
			}
		}	
	}
	
	/**
	 * Initialize the apply zone
	 */
	private void createApplyZone()
	{
		for(int i=0; i < 2; i++)
		{
			this.applyZone.add(new Block(null, null));
		}
	}

	/**
	 * Empty the arrays
	 */
	private void clear() 
	{
		this.equationLeft.clear();
		this.equationRight.clear();
		this.operations.clear();
		this.operator.clear();
		this.pile.clear();
		this.applyZone.clear();
		this.popup = null;
	}

	/**
	 * Update the arrays according to the output from the manager
	 */
	public void updateLevelContent() 
	{
		String s[] = GameLevelManager.getInstance().getCurrentGameLevelStringsWithSeparators();
		clear();
		createEquation(s[1], s[2]);
		createPile(s[4]);
		createOperators(s[3]);
		createApplyZone();
	}
}
