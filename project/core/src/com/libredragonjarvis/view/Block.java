package com.libredragonjarvis.view;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;

/**
 * A block is TextButton with additional info like the ID and the Value of the block
 *
 * @since 1.0
 */
public class Block extends TextButton{
	
	/**
	 * the ID of the block ex:'00'
	 */
	private String ID;
	
	/**
	 * the value of the block ex:'d'
	 */
	private String value;
	
	/**
	 * Get the ID of the block
	 * @return the ID of the block
	 */
	public String getID() {
		return ID;
	}
	
	/**
	 * Set the ID of the block
	 * @param ID
	 */
	public void setID(String ID) {
		this.ID = ID;
	}
	
	/**
	 * Get the value of the block
	 * @return the value of the block
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Set the value of the block
	 * @param value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Create a block
	 * @param ID
	 * @param value
	 */
	public Block(String ID, String value)
	{
		super(value, new TextButtonStyle(null, null, null, new BitmapFont()));
		this.ID = ID;
		this.value = value;
	}
}
