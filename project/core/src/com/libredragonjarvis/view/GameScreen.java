package com.libredragonjarvis.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;

/**
 * GameScreen which display the game with the currently loaded level
 *
 * @since 1.0
 */
public class GameScreen implements Screen{
	
	/**
	 * The game instance
	 */
	private LibreDragonJarvis game;
	
	/**
	 * Level instance which contains the data of the level
	 */
	private Level level;
	
	/**
	 * LevelRenderer instance which will render the level
	 */
	private LevelRenderer renderer;
	
	/**
	 * width of the window
	 */
	private float width;
	
	/**
	 * height of the window
	 */
	private float height;
	
	/**
	 * GameScreen constructor
	 * @param the game instance
	 */
	GameScreen(LibreDragonJarvis gam)
	{
		this.game = gam;
		
		//set the width and the height of the window
		this.width = (float)1600;
		this.height = (float)900;
		
		//create the level
		level = new Level();
		
		//set the renderer
		renderer = new LevelRenderer(game, level, false, this.width, this.height);
		
		//subscribe the renderer as a model observer
		renderer.subscribeInstanceAsModelObserver();
	}
	
	/**
	 * Function called when the screen is shown
	 */
	@Override
	public void show() {
		Gdx.input.setInputProcessor(renderer.getStage());
	}

	/**
	 * Renderer the element of the level
	 * @param delta the time span between the current frame and the last frame in seconds.
	 */
	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0.1f, 0.1f, 0.1f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		renderer.render(delta);
	}

	/**
	 * Adapt the level to window size
	 * @param width new screen width
	 * @param height new screen height
	 */
	@Override
	public void resize(int width, int height) {
		renderer.getStage().getViewport().setScreenSize(width, height);
		renderer.getStage().getViewport().update(width, height, false);
	}

	/**
	 * Function not used 
	 */
	@Override
	public void pause() {}

	/**
	 * Function not used 
	 */
	@Override
	public void resume() {}

	/**
	 * Function not used 
	 */
	@Override
	public void hide() {}

	/**
	 * Free the memory and the input processor
	 */
	@Override
	public void dispose() {
		Gdx.input.setInputProcessor(null);
		renderer.dispose();
	}
}
