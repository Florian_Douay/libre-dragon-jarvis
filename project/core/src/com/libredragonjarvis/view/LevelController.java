package com.libredragonjarvis.view;

import managers.GameLevelManager;
import managers.GameManager;
import managers.RulesApplicationManager;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.ui.Window;

/**
 * LevelController
 * Create every listener for the stage component 
 * 
 * @since 1.0
 */
public class LevelController {
	
	/**
	 * The game instance
	 */
	private final LibreDragonJarvis game;
	
	/**
	 * The current level
	 */
	private Level level;
	
	/**
	 * Reference to the current levelRenderer 
	 */
	private LevelRenderer renderer;
	
	/**
	 * LevelController constructor, 
	 * initialize every listener
	 * @param _game game instance
	 * @param level current level
	 * @param renderer current levelRenderer
	 */
	LevelController(LibreDragonJarvis _game, Level level, LevelRenderer renderer)
	{
		this.game = _game;
		this.level = level;
		this.renderer = renderer;
		createButtonsListeners();
		createEquationListener();
		createOperatorListener();
		createPileListener();
	}
	
	/**
	 * Show the Popup with the list of operations for the block clicked
	 * @param block Block clicked
	 * @param operations List of possible operations
	 */
	private void showPopup(Block block, String[][] operations)
	{
		for(final Actor actor : renderer.getStage().getActors())
		{
			if(actor.getName() == "Popup")
			{
				level.getPopup().setWindow((Window)actor);
				level.getPopup().updateTable(operations, block.getID(), block.getValue());
				actor.setPosition(renderer.getStage().getWidth()-(renderer.getStage().getWidth()/2)-actor.getWidth()/2, renderer.getStage().getHeight()/18);
				actor.setVisible(true);
			}
		}
	}
	
	/**
	 * Create listeners for the Quit, Reload, Equal, Apply and Clear button
	 */
	private void createButtonsListeners()
	{
		for(final Actor actor : renderer.getStage().getActors())
		{
			//Return to main menu
			if(actor.getName() == "Quit")
			{
				actor.addListener(new ClickListener(){
					@Override
					public void clicked(InputEvent event, float x, float y)
					{
						GameLevelManager.getInstance().restartCurrentGameLevel();
						GameLevelManager.getInstance().unloadCurrentGameLevel();
						game.setScreen(new MainMenuScreen(game));
					}
				});
			}
			//Reload current level
			if(actor.getName() == "Reload")
			{
				actor.addListener(new ClickListener(){
					@Override
					public void clicked(InputEvent event, float x, float y)
					{
						GameLevelManager.getInstance().restartCurrentGameLevel();
						game.setScreen(new GameScreen(game));
					}
				});
			}
			//Apply a global rule
			if(actor.getName() == "Equal")
			{
				actor.addListener(new ClickListener(){
					@Override
					public void clicked(InputEvent event, float x, float y)
					{
						String[][] operations = RulesApplicationManager.getInstance().getPossibleGlobalFormChanges();
						level.updateOperations(operations);
						showPopup(new Block("2", "="), operations);
					}
				});
			}
			//Apply a global rule with a given operator
			if(actor.getName() == "Apply")
			{
				actor.addListener(new ClickListener(){
					@Override
					public void clicked(InputEvent event, float x, float y)
					{
						if(level.getApplyZone().get(0).getValue() != null)
						{
							//check the arity of the operator
							int arity = GameManager.getInstance().getOperatorArity(level.getApplyZone().get(0).getValue());
							String s[] = new String[arity-1];
							if(arity == 2)
							{
								s[0] = level.getApplyZone().get(1).getValue();
								if(level.getApplyZone().get(1).getValue() != null)
								{
									RulesApplicationManager.getInstance().applyOperation(level.getApplyZone().get(0).getValue(), s);
								}
							}
							else
							{
								RulesApplicationManager.getInstance().applyOperation(level.getApplyZone().get(0).getValue(), s);
							}
						}
					}
				});
			}
			//Empty the Apply zone
			if(actor.getName() == "Clear")
			{
				actor.addListener(new ClickListener(){
					@Override
					public void clicked(InputEvent event, float x, float y)
					{
						level.getApplyZone().get(0).setVisible(false);
						level.getApplyZone().get(0).setValue(null);
						level.getApplyZone().get(0).setID(null);
						level.getApplyZone().get(0).setText(null);
						
						level.getApplyZone().get(1).setVisible(false);
						level.getApplyZone().get(1).setValue(null);
						level.getApplyZone().get(1).setID(null);
						level.getApplyZone().get(1).setText(null);
					}
				});
			}
		}
	}
	
	/**
	 * Create listeners for every block in the current level
	 */
	private void createEquationListener()
	{
		for(final Block block : level.getEquationLeft())
		{
			block.addListener(new ClickListener(){
				@Override
				public void clicked(InputEvent event, float x, float y)
				{
					String[][] operations = RulesApplicationManager.getInstance().getPossibleFormChanges(block.getID());
					level.updateOperations(operations);
					showPopup(block, operations);
				}
			});
		}
		for(final Block block : level.getEquationRight())
		{
			block.addListener(new ClickListener(){
				@Override
				public void clicked(InputEvent event, float x, float y)
				{
					String[][] operations = RulesApplicationManager.getInstance().getPossibleFormChanges(block.getID());
					level.updateOperations(operations);
					showPopup(block, operations);
				}
			});
		}
	}
	
	/**
	 * Create the input listeners for every operators
	 */
	private void createOperatorListener()
	{
		for(final Block block : level.getOperator())
		{
			block.addListener(new ClickListener(){
				@Override
				public void clicked(InputEvent event, float x, float y)
				{
					level.getApplyZone().get(0).setVisible(true);
					level.getApplyZone().get(0).setValue(block.getValue());
					level.getApplyZone().get(0).setID(block.getID());
					level.getApplyZone().get(0).setText(level.getApplyZone().get(0).getValue());
				}
			});
		}
	}
	
	/**
	 * Create the input listeners for every block in the pile
	 */
	private void createPileListener()
	{
		for(final Block block : level.getPile())
		{
			block.addListener(new ClickListener(){
				@Override
				public void clicked(InputEvent event, float x, float y)
				{
					if(level.getApplyZone().get(0) != null)
					{
						int arity = GameManager.getInstance().getOperatorArity(level.getApplyZone().get(0).getValue());
						if(arity == 2)
						{
							level.getApplyZone().get(1).setVisible(true);
							level.getApplyZone().get(1).setValue(block.getValue());
							level.getApplyZone().get(1).setID(block.getID());
							level.getApplyZone().get(1).setText(level.getApplyZone().get(1).getValue());
						}
					}
				}
			});
		}
	}
}
