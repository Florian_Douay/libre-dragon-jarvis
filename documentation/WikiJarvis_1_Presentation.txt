## **LIBRE DRAGON JARVIS**
### Présentation
LibreDragonJarvis est un logiciel dédié à l'apprentissage de la logique mathématique.
L'utilisateur lambda doit manipuler une équation mathématique dans le but de la résoudre.

Deux types d'utilisateurs existes : les utilisateurs et les superUtilisateurs.
La dénomination de superUtilisateur désigne une personne qui va modifier les fichiers de configuration des niveaux et des opérateurs dans le but de, par exemple, créer un ensemble de niveaux par ordre croissant de difficultés.

Chaque niveau est constitué d'une inconnue x ainsi que de variables, de constantes et d'opérateurs.
L'objectif pour l'utilisateur est d'isoler le x dans chaque niveau et ainsi résoudre l'équation.
Pour cela, il aura à sa disposition une série d'opérations mathématiques à appliquer.

*Projet de Génie Logiciel dans le cadre du Master 1 Informatique, Faculté des Sciences de Luminy*

### Contacts

|Enseignants				|				|
|:--------------------------------------|:------------------------------|
|Kévin Perrot				|kevin.perrot+m1swe@gmail.com	|
|Pablo Arrighi				|pablo.arrighi@univ-amu.fr	|

|Auteurs: **Team Jarvis** 	|				|						|
|:------------------------------|:------------------------------|:----------------------------------------------|
|Jules Cases			|jules.cazes@gmail.com		|Modèle métier					|
|Florian Douay			|douay.florian@gmail.com	|Manager, Modèle technique			|
|Charbel Fourel			|le.tcharb@gmail.com		|Architecture, Process				|
|Martrice Pierre-Antoine	|panim.martrice@gmail.com	|Modèle métier, documentation			|
|Julien Vallet			|julien.vallet@outlook.com	|Modèle technique, vue ligne de commande	|
|Raphaël Onfroy			|onfroy.raphael@gmail.com	|Vue libGDX					|

***
*M1 Informatique, Promotion 2015/2016, Faculté des Sciences de Luminy*

*Team Jarvis*



|Auteurs: **Team Jarvis** 
|Jules Cases : Modèle Métier
|Florian Douay : Manager, modèle technique
|Charbel Fourel : architecture, preocess
|Martrice Pierre-Antoine : modèle métier, documentation
|Julien Vallet : modèle technique, vue ligne de commande
|Raphaël Onfroy : vue libGDX















